&emsp;

> **作者**：莫明缙 (中山大学)    
> **E-Mail:** <1061995117@qq.com>  

[toc]

## 0.简介

本文根据 de Chaisemartin, C. and D'Haultfoeuille (2019) ，介绍了 `did_multiplegt` 命令的使用情景和方法，解释了传统双向固定效应回归在异质性处理效果下的有偏性，阐明了 **$\mathrm{DID}_{\mathrm{M}}$** 估计量的优良特性，并给出了 `did_multiplegt` 命令的一个 Stata 实例。

## 1. 为什么要使用 did_multiplegt ？

### 1.1 did_multiplegt 与被广泛应用的 DID 多期多个体回归方法

在研究中，我们常常需要衡量某种“处理”对某些群体的效果，比如修建高铁对城市 GDP 的影响等。一种常见的方法是比较在不同时间内接受处理的群体的变化；在实践中，这可以由控制了群体固定效应和时间固定效应的回归 (下文称之为双向固定效应回归) 来实现。

de Chaisemartin, C. and D'Haultfoeuille (2019) 发现， 2010 到 2012 年间 20% 的 AER 文章都使用了双向固定效应回归方法，足见其应用之广。

然而，传统的双向固定效应回归方法存在一定的缺陷：它依赖于处理效果不随群体和时间改变而变化的强假定，而这个假定常常不能被满足 (出现异质性处理效果) ，此时，其估计量将会存在有偏的问题 (下个小节会集中讨论这个问题) 。

于是，de Chaisemartin, C. and D'Haultfoeuille (2019) 构造了一个新的估计量 **$\mathrm{DID}_{\mathrm{M}}$** 来满足异质性处理效果下对估计量无偏性的要求 (2.1 展示了它的构建与性质)。这个估计量可以通过 `did_multiplegt` 来计算。

### 1.2 传统双向固定效应回归的受限

#### 1.2.1 模型设定

考虑分为 $G$ 个群体和 $T$ 个时期的观测数据。对于 $(g, t) \in \{1, \ldots, G\} \times\{1, \ldots, T\}$ ， 令 $N_{gt}$ 代表时期 $t$ 和群体 $g$ 内的观测数量，令 $N$ 代表总观测数。令 $D_{i, g, t}$ 、 $\left(Y_{i, g, t}(0), Y_{i, g, t}(1)\right)$ 和 $Y_{i, g, t}=Y_{i, g, t}\left(D_{i, g, t}\right)$ 分别代表时期 $t$ 和群体 $g$ 内的个体 $i$ 的处理状态、有无处理状态下的潜在观测结果和实际观测结果。

又有对所有的 $(g, t)$ ，有
$$
D_{g, t}=\frac{1}{N_{g, t}} \sum_{i=1}^{N_{g, t}} D_{i, g, t}, Y_{g, t}(0)=\frac{1}{N_{g, t}} \sum_{i=1}^{N_{g, t}} Y_{i, g, t}(0), Y_{g, t}(1)=\frac{1}{N_{g, t}} \sum_{i=1}^{N_{g, t}} Y_{i, g, t}(1), \text { and } Y_{g, t}=\frac{1}{N_{g, t}} \sum_{i=1}^{N_{g, t}} Y_{i, g, t}
$$

下面提出四条本文始终遵循的基本假定。

假定一 (平衡面板群体假定) ：对于所有 $(g, t) \in\{1, \ldots, G\} \times\{1, \ldots, T\}, N_{g, t}>0$ 。

假定二 (锐性假设) ：对于所有 $(g, t) \in\{1, \ldots, G\} \times\{1, \ldots, T\}$ and $i \in\left\{1, \ldots, N_{g, t}\right\}$, $D_{i, g, t}=D_{g, t}$ 。

假定三 (独立群体假定) ：向量 $\left(Y_{g, t}(0), Y_{g, t}(1), D_{g, t}\right)_{1 \leq t \leq T}$ 是相互独立的。

假定四 (强的外生假定) ：对于所有 $(g, t) \in\{1, \ldots, G\} \times\{2, \ldots, T\}$, $E\left(Y_{g, t}(0)-Y_{g, t-1}(0) \mid D_{g, 1}, \ldots, D_{g, T}\right)=E\left(Y_{g, t}(0)-Y_{g, t-1}(0)\right)$ 。

下面给出双向固定效应回归的相关表达。

令 $\widehat{\beta}_{\text {fe }}$ 代表 **$D_{g, t}$** 在 $Y_{i, g, t}$ 对群体固定效应、时间固定效应和 **$D_{g, t}$** 的回归中的系数。令 $\beta_{f e}=E\left[\widehat{\beta}_{f e}\right] .$ 。

对于任意的变量 **$X_{g, t}$** ，令 $X_{g, .}= \sum_{t=1}^{T}\left(N_{g, t} / N_{g,}\right) X_{g, t}$ ，$X_{., t}=\sum_{g=1}^{G}\left(N_{g, t} / N_{., t}\right) X_{g, t}$ ，$X_{\ldots}=\sum_{g, t}\left(N_{g, t} / N\right) X_{g, t}$ 。令 **$X$** 代表向量 $\left(X_{g, t}\right)(g, t) \in\{1, \ldots, G\} \times\{1, \ldots, T\}$ 。

我们在假设五的基础上研究双向固定效应回归。

假设五 (共同趋势) ：对于 $t \geq 2, E\left(Y_{g, t}(0)-Y_{g, t-1}(0)\right)$ 不随 $g$ 变化而变化。

首先构建我们想要得到的“处理效果的期望”。令

$$
\Delta^{T R}=\frac{1}{N_{1}} \sum_{(i, g, t): D_{g, t}=1}\left[Y_{i, g, t}(1)-Y_{i, g, t}(0)\right]
$$

其中，$N_{1}=\sum_{i, g, t} D_{i, g, t}$ 代表被处理的个体的总数， $\Delta^{T R}$ 代表被处理个体的平均处理效果，进而令 $\delta^{T R}=E\left[\Delta^{T R}\right]$ 代表其期望，也即是代表我们想要得到的处理效果期望。

又令

$$
\Delta_{g, t}=\frac{1}{N_{g, t}} \sum_{i=1}^{N_{g, t}}\left[Y_{i, g, t}(1)-Y_{i, g, t}(0)\right]
$$

$\Delta_{g, t}$ 代表了在时期 $t$ 和群体 $g$ 内的平均处理效果。不难得到：

$$
\delta^{T R}=E\left[\sum_{g, t: D_{g, t}=1} \frac{N_{g, t}}{N_{1}} \Delta_{g, t}\right] \quad (1)
$$

下面开始构造双向固定效应回归得到的参数 $\beta_{f e}$ 的表达形式并将之与 $\delta^{T R}$进行对比。

令 $\varepsilon_{g, t}$ 代表 $D_{g, t}$ 对群体固定效应和时间固定效应回归的随机误差项。

$$
D_{g, t}=\alpha+\gamma_{g}+\lambda_{t}+\varepsilon_{g, t}
$$

接着，我们构建参数 $w_{g, t}$：

$$
w_{g, t}=\frac{\varepsilon_{g, t}}{\sum_{(g, t): D_{g, t}=1} \frac{N_{g, t}}{N_{1}} \varepsilon_{g, t}}
$$

可以证明，如果假定一到假定五满足的话，有：

**定理一**

$$
\beta_{f e}=E\left[\sum_{(g, t): D_{g, t}=1} \frac{N_{g, t}}{N_{1}} w_{g, t} \Delta_{g, t}\right] \quad (2)
$$

将 (1) 和 (2) 进行对比，可见一般上，$\beta_{f e} \neq \delta^{T R}$ 。从而， $\widehat{\beta}_{f e}$ 是一个有偏的估计量。在下一节中，我们通过一个简单的例子直观地看到这一点。

#### 1.2.2 有偏性：一个简单的例子

考虑包含两个组和三期的观测。其中，组 1 只在第三期被处理，而组 2 则在第二和第三期都被处理了。假定 $N_{g, t} / N_{g, t-1}$ 不随时间变化，则可以证明：

$$
\varepsilon_{g, t}=D_{g, t}-D_{g, .}-D_{., t}+D_{., .}
$$

即：

$$
\begin{array}{l}
\varepsilon_{1,3}=1-1 / 3-1+1 / 2=1 / 6 \\
\varepsilon_{2,2}=1-2 / 3-1 / 2+1 / 2=1 / 3 \\
\varepsilon_{2,3}=1-2 / 3-1+1 / 2=-1 / 6
\end{array}
$$

根据 (2) ，有：

$$
\beta_{f e}=1 / 2 E\left[\Delta_{1,3}\right]+E\left[\Delta_{2,2}\right]-1 / 2 E\left[\Delta_{2,3}\right]
$$

可见， $\beta_{f e}$ 是被处理组在三个被处理的时期的处理效果的加权平均值。然而，权重并不恒等于 $1 / 3$ ——被处理的 $(g,t)$ 元胞总数的倒数。因此，一般来说 $\beta_{f e} \neq \delta^{T R}$ 。

更严重的是，并不是所有的权重都是正数：组 2 在第三期的平均处理效果的权重 $-1/6$ 就是严格的负数。

这样的负权重在存在异质性处理效果的时候将会带来难以接受的结果。为了展示这一点，考虑一种异质性处理效果： $E\left[\Delta_{1,3}\right]=E\left[\Delta_{2,2}\right]=1$ ， $E\left[\Delta_{2,3}\right]=4$ 。

在这种情况下，各个组接收到的都是严格的正的处理效果，其中组 2 在第三个时期接收到了一个较大的平均处理效果。此时有：

$$
\beta_{f e}=1 / 2 \times 1+1-1 / 2 \times 4=-1 / 2
$$

当所有处理效果都是正的时候， $\beta_{f e}$ 是严格的负数！因此， $\beta_{f e}$ 变成了一个相当具有误导性的估计量。

这个简单的例子反映了传统双向固定效应回归方法与异质性处理效果之间的不兼容。

双向固定效应回归的估计量是平均处理效果的加权平均值，而其权重可能为正，也可能为负。此时如果处理效果存在异质性，那么其估计量会存在一定的偏误。

反而，如果处理效果在群体和时间上都不存在变化，如在上述例子中令 $E\left[\Delta_{1,3}\right]=E\left[\Delta_{2,2}\right]=E\left[\Delta_{2,3}\right]=1$ ，那么有 $\beta_{f e}=1=\delta^{T R}$ ，即 $\beta_{f e}$ 是一个无偏的估计量。

#### 1.2.3 负权重：一般化

现在我们对造成有偏性的负权重问题进行一般化的阐述。

**命题一**：如果假定一成立， $t \geq 2$ 且 $\mathrm{~N}_{g, t} / N_{g, t-1}$ 不随组别变化，那么对于所有满足 $D_{g, t}=D_{g, t^{\prime}}=1$ 的 $\left(g, t, t^{\prime}\right)$ ， $D_{., t}>D_{., t^{\prime}}$ 意味着 $w_{g, t} < w_{g, t^{\prime}}$ 。同样地，对于所有满足t $D_{g, t}=D_{g^{\prime}, t}=1$ 的 $\left(g, g^{\prime}, t\right)$ ， $D_{g, .}>D_{g^{\prime}, .}$ 意味着$w_{g, t} < w_{g^{\prime}, t}$ 。

命题一表明，负权重更容易被赋予给那些有多个群体接受了处理的时期，以及那些已经接受了很多期处理的群体。

#### 1.2.4 渐进式处理 (Staggered adoption design) 的情况

定理六（渐进式处理）：对于所有的 $g$ ，以及所有的 $t > 2$ ，有$D_{g, t} \geq D_{g, t-1}$ 。

一个群体接受的处理随着时间逐渐地增加的情况被称为“渐进式处理”(Staggered adoption design) 。命题一在渐进式处理中有一些有趣的应用。

Borusyak and Jaravel (2017) 发现，在渐进式处理中，负权重更容易被赋予最后一期的处理效果。

这其实是命题一的一种特殊情况：在渐进式处理中， $D_{., t}$ 随时间递增，即接受处理的组的数量随时间递增，最后一期拥有最多的接受处理的组。

### 1.3 小结

在存在异质性处理效果的情况下，双向固定回归所赋予平均处理效果的负权重将会导致 $\beta_{f e}$ 的有偏性问题。因此，我们需要一个能够与异质性处理效果兼容的估计量，那就是下文将要介绍的 **$\mathrm{DID}_{\mathrm{M}}$** 估计量。

## 2. 如何使用 did_multiplegt ？

### 2.1 $\mathrm{DID}_{\mathrm{M}}$ 估计量的构建
本节我们构建能够克服异质性处理效果的 **$\mathrm{DID}_{\mathrm{M}}$** 估计量。

首先，令

$$
\delta^{S}=E\left[\frac{1}{N_{S}} \sum_{(i, g, t): t \geq 2, D_{g, t} \neq D_{g, t-1}}\left[Y_{i, g, t}(1)-Y_{i, g, t}(0)\right]\right]
$$

其中 $N_{S}=\sum_{(g, t): t \geq 2, D_{g, t} \neq D_{g, t-1}} N_{g, t}$ 。 $\delta^{S}$ 是经历处理状态转换 (下称经历转换) 的元胞的平均处理效果。

在以下四个补充假定成立的情况下， $\delta^{S}$ 能被 **$\mathrm{DID}_{\mathrm{M}}$** 估计量无偏地估计：

假设九 ( $Y(1)$ 的强外生性) ：对于所有的 $(g, t) \in\{1, \ldots, G\} \times\{2, \ldots, T\}$ ，有 $E\left(Y_{g, t}(1)-Y_{g, t-1}(1) \mid D_{g, 1}, \ldots, D_{g, T}\right)=E\left(Y_{g, t}(1)-Y_{g, t-1}(1)\right)$ 。

假设十 ( $Y(1)$ 的共同趋势) ：对于所有的 $t \geq 2$ ， $E\left(Y_{g, t}(1)-Y_{g, t-1}(1)\right)$ 不随组别而变化。

假设九和假设十分别对应了假设四和假设五。

假设十一 (稳定组群的存在)：对于所有的 $t \in\{2, \ldots, T]$ ，有

- 如果至少有一个组群 $g \in\{1, \ldots, G\}$ 满足$D_{g, t-1}=0$ 且 $D_{g, t}=1$ ，那么至少有一个组群 $g^{\prime} \neq g$ ， $g^{\prime} \in\{1, \ldots, G\}$ 满足 $D_{g^{\prime}, t-1}=D_{g^{\prime}, t}=0$ 。

- 如果至少有一个组群 $g \in\{1, \ldots, G\}$ 满足$D_{g, t-1}=1$ 且 $D_{g, t}=0$ ，那么至少有一个组群 $g^{\prime} \neq g$ ， $g^{\prime} \in\{1, \ldots, G\}$ 满足 $D_{g^{\prime}, t-1}=D_{g^{\prime}, t}=1$ 。

假定十二 (某一组群的处理结果和其它组群的处理状态的均值独立性) ：对于所有的 $g$ 和 $t$ ， $E\left(Y_{g, t}(0) \mid \boldsymbol{D}\right)=E\left(Y_{g, t}(0) \mid \boldsymbol{D}_{g}\right)$ ，$E\left(Y_{g, t}(1) \mid \boldsymbol{D}\right)=E\left(Y_{g, t}(1) \mid \boldsymbol{D}_{g}\right)$ 。其中 $\boldsymbol{D}_{g}=\left(D_{1, g}, \ldots, D_{T, g}\right)$ 。

对于所有的 $t \in\{2, \ldots, T\}$ 和所有的 $\left(d, d^{\prime}\right) \in\{0,1\}^{2}$ ，令

$$
N_{d, d^{\prime}, t}=\sum_{g: D_{g, t}=d, D_{g, t-1}=d^{\prime}} N_{g, t}
$$

现在我们可以构建 **$\mathrm{DID}_{\mathrm{M}}$** 。

令

$$
\mathrm{DID}_{+, t}=\sum_{g: D_{g, t}=1, D_{g, t-1}=0} \frac{N_{g, t}}{N_{1,0, t}}\left(Y_{g, t}-Y_{g, t-1}\right)-\sum_{g: D_{g, t}=D_{g, t-1}=0} \frac{N_{g, t}}{N_{0,0, t}}\left(Y_{g, t}-Y_{g, t-1}\right)
$$

$$
\mathrm{DID}_{-, t}=\sum_{g: D_{g, t}=D_{g, t-1}=1} \frac{N_{g, t}}{N_{1,1, t}}\left(Y_{g, t}-Y_{g, t-1}\right)-\sum_{g: D_{g, t}=0, D_{g, t-1}=1} \frac{N_{g, t}}{N_{0,1, t}}\left(Y_{g, t}-Y_{g, t-1}\right)
$$

最后，令

$$
\mathrm{DID}_{\mathrm{M}}=\sum_{t=2}^{T}\left(\frac{N_{1,0, t}}{N_{S}} \mathrm{DID}_{+, t}+\frac{N_{0,1, t}}{N_{S}} \mathrm{DID}_{-, t}\right)
$$

**定理二**：可以证明，如果假设一、二、四、五和九至十二成立的话， $E\left[D I D_{M}\right]=\delta^{S}$ ，且如果组群数量趋于无穷的话， **$\mathrm{DID}_{\mathrm{M}}$** 是 $\delta^{S}$ 的一个一致的和渐进正态的估计量。

下面来阐述定理二背后的直觉。 $\mathrm{DID}_{+, t}$ 比较了“进入处理者”和“仍未被处理者”两类组群从 $t-1$ 到 $t$ 时期的结果变化。在假设四和五成立的情况下，它估计了“进入处理者”的处理效果。

相应地， $\mathrm{DID}_{-, t}$ 对比了“退出处理者”和“保持处理者”从 $t-1$ 到 $t$ 时期的结果变化。在假设九和十成立的情况下，它衡量了“退出者”3的处理效果。

### 2.2 $\mathrm{DID}_{\mathrm{M}}$ 估计量的特性

虽然 **$\mathrm{DID}_{\mathrm{M}}$** 能够克服异质性处理效果的问题，但是在双向固定效应回归的估计量和 **$\mathrm{DID}_{\mathrm{M}}$** 的选择中，存在“误差-方差”交易。

即使在存在异质性处理效果的情况下，双向固定效应回归的估计量的方差有时比 **$DID_{M}$** 的要大，但在一般的情形下， **$\mathrm{DID}_{\mathrm{M}}$** 仍然是方差更大的那个。

### 2.3 安慰剂检验：假设四、五、九和十成立吗？

 **$\mathrm{DID}_{\mathrm{M}}$** 使用处理状态“稳定”的组群的趋势来揭示经历转换的组群的趋势。如果转换者的趋势与稳定者的不同，那么这个策略会失败。使用安慰剂检验可以检测这个问题，这个检验比较了 $t$ 期经历和没有经历转换的组群在 $t-2$ 到 $t-1$ 期的结果变化。

 这个检验依赖于假定十三：

 假定十三 (存在可以用于安慰剂检验的“稳定”组群)：对于所有的 $t \in\{3, \ldots, T\}$ ，有

- 如果至少有一个组群 $g \in\{1, \ldots, G\}$ 满足 $D_{g, t-2}=D_{g, t-1}=0$ 且 $D_{g, t}=1$ ，那么至少有一个组群 $g^{\prime} \neq g$ ， $g^{\prime} \in\{1, \ldots, G\}$ 满足 $D_{g^{\prime}, t-2}=D_{g^{\prime}, t-1}=D_{g^{\prime}, t}=0$ 。
    
- 如果至少有一个组群 $g \in\{1, \ldots, G\}$ 满足 $D_{g, t-2}=D_{g, t-1}=1$ 且 $D_{g, t}=0$ ，那么至少有一个组群 $g^{\prime} \neq g$ ， $g^{\prime} \in\{1, \ldots, G\}$ 满足 $D_{g^{\prime}, t-2}=D_{g^{\prime}, t-1}=D_{g^{\prime}, t}=1$ 。

对于所有的 $t \in\{2, \ldots, T\}$ 和 $\left(d, d^{\prime}, d^{\prime \prime}\right) \in\{0,1\}^{3}$ ，令

$$
N_{d, d^{\prime}, d^{\prime \prime}, t}=\sum_{g: D_{g, t}=d, D_{g, t-1}=d^{\prime}, D_{g, t-2}=d^{\prime \prime}} N_{g, t}
$$

准备构建安慰剂。令：

$$
N_{S}^{\mathrm{pl}}=\sum_{(g, t): t \geq 3, D_{g, t} \neq D_{g, t-1}=D_{g, t-2}} N_{g, t}
$$

$$
\mathrm{DID}_{+, t}^{\mathrm{pl}}=\sum_{g: D_{g, t}=1, D_{g, t-1}=D_{g, t-2}=0} \frac{N_{g, t}}{N_{1,0,0, t}}\left(Y_{g, t-1}-Y_{g, t-2}\right)-\sum_{g: D_{g, t}=D_{g, t-1}=D_{g, t-2}=0} \frac{N_{g, t}}{N_{0,0,0, t}}\left(Y_{g, t-1}-Y_{g, t-2}\right)
$$

$$
\mathrm{DID}_{-, t}^{\mathrm{pl}}=\sum_{g: D_{g, t}=D_{g, t-1}=D_{g, t-2}=1} \frac{N_{g, t}}{N_{1,1,1, t}}\left(Y_{g, t-1}-Y_{g, t-2}\right)-\sum_{g: D_{g, t}=0, D_{g, t-1}=D_{g, t-2}=1} \frac{N_{g, t}}{N_{0,1,1, t}}\left(Y_{g, t-1}-Y_{g, t-2}\right)
$$

构建安慰剂估计量：

$$
\mathrm{DID}_{\mathrm{M}}^{\mathrm{pl}}=\sum_{t=3}^{T}\left(\frac{N_{1,0,0, t}}{N_{S}^{\mathrm{pl}}} \mathrm{DID}_{+, t}^{\mathrm{pl}}+\frac{N_{0,1,1, t}}{N_{S}^{\mathrm{pl}}} \mathrm{DID}_{-, t}^{\mathrm{pl}}\right)
$$

**定理三**：如果假定一，二，四，五，九，十，十二和十三成立，那么 $E\left[D I D_{M}^{p l}\right]=0$ 。

具体而言，如果假定四和五成立，那么 $E\left[\mathrm{DID}_{+, t}^{\mathrm{pl}}\right]=0$ 。

如果假定九和十成立，那么 $E\left[\mathrm{DID}_{-, t}^{\mathrm{pl}}\right]=0$ 。

因此， $E\left[\mathrm{DID}_{\mathrm{M}}^{\mathrm{pl}}\right]=0$ 是一个对假设四、五、九和十的有效检验。如果它显著不等于零，这意味着有些假定被违背了，即有些转换组在转换前经历着与稳定组不同的趋势。

我们也可以采取滞后更多期的方法来构建安慰剂估计量。 $\mathrm{DID}_{\mathrm{M}}^{\mathrm{pl}}$ 和其他安慰剂都可以通过 `did_multiplegt` 来实现。

### 2.4 $\mathrm{DID}_{\mathrm{M}}$ 估计量的拓展

Clémentde Chaisemartin† and Xavier D’Haultfœuille‡ (2019) 在第五节中对 **$\mathrm{DID}_{\mathrm{M}}$** 估计量和双向固定效应回归的估计量作了拓展的分析。

首先，双向固定效应回归的估计量的解构分析可以被拓展到模糊设计、非二元处理和带控制变量的双向固定效应回归中，结论不变。其次， **$\mathrm{DID}_{\mathrm{M}}$** 估计量也可以很容易地被拓展到非二元处理中。

使用 `twowayfeweights` 、 `fuzzydid` 和 `did_multiplegt` 能实现以上拓展。

### 2.5 did_multiplegt 的使用方法

命令输入格式：

```
did_multiplegt Y G T D [if] [in] [, placebo(#) dynamic(#) controls(varlist) trends_nonparam(varlist) trends_lin(varlist) weight(varlist) breps(#) cluster(varname) covariances average_effect(string) recat_treatment(varlist) threshold_stable_treatment(#) save_results(path) ]
```

其中， Y 是结果变量， G 是组别变量， T 是时间变量， D 是处理状态变量。

在 options 中， placebo 指定了安慰剂的个数， dynamic 指定了在渐进式处理中需要估计的动态处理效果的数量， breps 决定了是否返回估计量的标准误。

如果指定了 breps 选项，则该命令将返回一个包含所有估计处理效果和安慰剂效果的图形，以及使用正态近似构建的95%置信区间。

回归的结果将会保留在 e() 中。

## 3. Stata 实例 (以 Vella and Verbeek (1998) 为例)

许多文献探讨过工会成员资格对工资的影响。Vella and Verbeek (1998) 就是其中一篇。他们使用了来自 NLSY 的从 1980 到 1987 的美国男性的数据，得到了加入公会会使得工资增长约 21% 的结论。

遵循 de Chaisemartin, C. and D'Haultfoeuille (2019) 的做法，我们使用原数据集，并利用 `twowayfeweights` 和  `did_multiplegt` 命令来重新检验加入工会对工资的福利效应。

Stata 操作：

```
 ssc install bcuse
    bcuse wagepan
    twowayfeweights lwage nr year union, type(feTR)
```

`twowayfeweights` 的结果显示，传统的双向固定效应回归中，负权重的处理效果比例高达 20% 左右。如果工会的影响存在异质性，则该估计方法有偏。因此考虑使用  `did_multiplegt` 估计。

Stata 操作：

```   
    did_multiplegt lwage nr year union, placebo(1) breps(50) cluster(nr)
    ereturn list
```

结果显示：

```
  e(effect_0) =  .0261225951945861
  e(se_effect_0) =  .0194752588662298
  e(N_effect_0) =  3815
  e(N_switchers_effect_0) =  508
  e(placebo_1) =  .099321110943454
  e(se_placebo_1) =  .0315586535625052
  e(N_placebo_1) =  2842
  ```

结果中， e(effect_0) =  .026 表示工会效应约为 2.6% ，远小于 Vella and Verbeek (1998) 中估计的 20% ，这表明工会的影响确实存在异质性，传统的双向固定效应回归方法有偏。

e(placebo_1) =  .099 表明安慰剂估计量明显大于零，因此在该模型中，上文所提到的假设四、五、九和十中有些不成立，即有些工会状态转换的工人在转换前经历着与状态稳定的工人不同的工资趋势。

de Chaisemartin, C. and D'Haultfoeuille (2019) 指出，安慰剂估计表明 **$\mathrm{DID}_{\mathrm{M}}$** 所估计的工会效应也被高估了，因为转换前的趋势差异是正的。因此，本文估计的工会效应要小于 2.6% 。

同时， de Chaisemartin, C. and D'Haultfoeuille (2019) 表明，因为在“退出工会者”中，转换前的趋势差异几乎为零，其工会效应也十分接近零，所以明显的工会福利可能并不存在。

## 4. 总结

在存在异质性处理效果的情况下，双向固定回归所赋予平均处理效果的负权重将会导致其估计量的有偏性问题。

**$\mathrm{DID}_{\mathrm{M}}$** 估计量能够克服异质性处理效果问题，但可能会带来较大的方差。同时， **$\mathrm{DID}_{\mathrm{M}}$** 依赖于一系列共同趋势的假定。可以使用安慰剂来检验它们。

 `twowayfeweights` 可以检验双向固定效应回归中的负权重的比例，以确定是否需要采用 **$\mathrm{DID}_{\mathrm{M}}$** 估计量。  `did_multiplegt` 可以计算 **$\mathrm{DID}_{\mathrm{M}}$** 及其各种安慰剂。

 ## 5. 参考文献

- de Chaisemartin, C. and D'Haultfoeuille, X. Forthcoming, American Economic Review. Two-Way Fixed Effects Estimators with Heterogeneous Treatment Effects. [-PDF-](https://www.aeaweb.org/articles?id=10.1257/aer.20181169)

- Vella, F. and Verbeek, M. 1998. Journal of Applied Econometrics 13(2), 163–183.  Whose wages do unions raise? a dynamic model of unionism and wage rate determination for young men. [-PDF-](https://sci-hub.do/10.2307/223257)

