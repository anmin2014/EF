# 提纲
## （一）应用场景及理论推导
### 作者撰写的摘要：
reg2logit通过转换xvars上yvar的线性回归的OLS估计来估计yvar在xvars上的逻辑回归的参数。允许因子xvars。最初由Haggstrom（J.Bus.Econ.Stat。，1983）得出的变换公式由Allison（2020）讨论。假设xvar是基于yvar值的多态正态变量，则转换后的OLS估计是logistic回归的完全有效估计。如果xvar实际上是有条件的多元正态，则reg2logit产生的估计比logit命令产生的“无分布”估计更有效，后者不假设xvar的分布。
### 参考文献：
>Allison, P.D. (2020, April 24). Better predicted probabilities from linear probability models. Statistical Horizons blog.
[链接](https://statisticalhorizons.com/better-predicted-probabilities)

>Haggstrom, G. W. (1983). Logistic regression and discriminant analysis by ordinary least squares. Journal of Business & Economic Statistics, 1(3), 229-238.

>Ji, Z., & Telgarsky, M. (2018). Risk and parameter convergence of logistic regression. arXiv preprint arXiv:1803.07300.

>Minka, T. P. (2003). A comparison of numerical optimizers for logistic regression. Unpublished manuscript
[链接](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.85.7017&rep=rep1>&type=pdf.)

## （二）reg2logit语法介绍
### 下载方法
### 代码实现方式
### 下载方式

## （三）reg2logit模拟实例
采用sysuse auto的数据
或找到有条件的多元正态xvars的dataset