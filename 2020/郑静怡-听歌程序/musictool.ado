cap prog drop musictool
prog def  musictool
    version 16
	syntax [, NAME(string)] [PLATform(string)] [BRowse] [STOP] [KIND(string)]   //语法
	preserve //保存工作进度
	*使用了外部命令，如果没有则自动安装外部命令
	cap findfile jsonio.ado
	if  _rc>0 ssc install jsonio
	cap findfile sxpose.ado
	if  _rc>0 ssc install sxpose
	
	*停止播放本地音乐的模块
	if "`stop'" == "stop" {
		foreach exe in Music.UI cloudmusic QQMusic{ 
		!taskkill /f /im `exe'.exe
		}
		exit
	}	

	*默认网易云在线播放)
	if "`platform'" == "" {
	local platform = "Netease"
	} 
	
	*关于platform选择的模块
	if "`platform'" != "" {	
		if "`platform'" != "Netease" &  "`platform'" != "Tencent" &  "`platform'" != "Local" & "`platform'" != "netease" &  "`platform'" != "tencent" &  "`platform'" != "local" & "`platform'" != "NETEASE" &  "`platform'" != "TENCENT" &  "`platform'" != "LOCAL"{			
			noi di as error "The {bf:platform} you selected is not available in our package."
			noi di as error "Please choose Netease , Tencent or Local and try again"
			exit
		}	
	}

	*如果不写歌曲名，随机播放歌曲
	if "`name'" == "" {
	//实际上是网易云的飙升榜
	local platform="Netease"
	clear
	jsonio kv, file(http://music.163.com/api/playlist/detail?id=19723756)
	local num = int(runiform(1,100))
	qui keep if ustrregexm(key, "/result/tracks_`num'/name") == 1 | ustrregexm(key, "/result/tracks_`num'/id") == 1 | ustrregexm(key, "/result/tracks_`num'/artists_1/name") == 1
	local ID=value[2]     //歌曲ID
	local song_name=value[1] //歌曲中文名
	local singer=value[3] //歌手名字
	
	lyrics anything, id(`ID')
	
	//构造最终的URL
	local URL1="https://music.163.com/#/"
	local URL2="?id="
	if "`kind'"=="1" local kind = "song"
	if "`kind'"=="10" local kind = "album"
	if "`kind'"=="1000" local kind = "playlist"
	if "`kind'"=="1002" local kind = "user/home"
	if "`kind'"=="1004" local kind = "video"
	if "`kind'"=="1006" local kind = "song"
	if "`kind'"=="1009" local kind = "djradio"
	local URL="`URL1'`kind'`URL2'`ID'" 
	
	}
	else {
	mata:str = urlencode("`name'")
	mata:st_local("songname",str) //中文歌名转码
	
	
	*如果选择播放本地音乐	
	if "`platform'" == "Local" | "`platform'" == "local" | "`platform'" == "LOCAL" {
		if "$songpath"==""{
		dis as error "请定义音乐存放路径(global songpath),并以\结尾"
		exit
		}
	local filename = "$songpath"+"`name'"+".mp3"
	!start `filename'
	exit
	}

	else{
	*如果选择网易云	
	if "`platform'" == "Netease" | "`platform'" == "netease" | "`platform'" == "NETEASE" {
	*默认搜索单曲
	if "`kind'" == "" {
	local kind = "1"
	} //1,单曲；10,专辑；1000,歌单；1002,用户；1004,MV；1006,歌词；1009,主播电台
	
	local URL1="http://music.163.com/api/search/get/web?csrf_token=hlpretag=&hlposttag=&s="
	local URL2="&type="
	local URL3="&offset=0&total=true&limit=1"
	local URL="`URL1'`songname'`URL2'`kind'`URL3'" //构造获取歌曲信息的URL
	clear	
	jsonio kv, file(`URL') //用了外部命令jsonio,获取json文件
	
	*搜索的是单曲
	if "`kind'" == "1" {
	qui keep if ustrregexm(key, "/result/songs_1/id") == 1 | ustrregexm(key, "/result/songs_1/name") == 1 | ustrregexm(key, "/result/songs_1/artists_1/name") == 1
	local ID=value[1]     //歌曲ID
	local song_name=value[2] //歌曲中文名
	local singer=value[3] //歌手名字

	lyrics anything, id(`ID')
	
	} 
	
	*搜索的是歌单
	if "`kind'" == "1000" {
	local ID=value[1]	//歌单ID
	clear
	jsonio kv, file(http://music.163.com/api/playlist/detail?id=`ID')
	qui keep if ustrregexm(key, "/result/name") == 1 | ustrregexm(key, "/result/description") == 1 | ustrregexm(key, "/result/creator/nickname") == 1
	local description=value[2]     //简介
	local song_name=value[3] //歌单名字
	local singer=value[1] //创建人名字	
	} 

	//构造最终的URL
	local URL1="https://music.163.com/#/"
	local URL2="?id="
	if "`kind'"=="1" local kind = "song"
	if "`kind'"=="10" local kind = "album"
	if "`kind'"=="1000" local kind = "playlist"
	if "`kind'"=="1002" local kind = "user/home"
	if "`kind'"=="1004" local kind = "video"
	if "`kind'"=="1006" local kind = "song"
	if "`kind'"=="1009" local kind = "djradio"
	local URL="`URL1'`kind'`URL2'`ID'" 
	
	}

	*如果选择QQ音乐
	if "`platform'" == "Tencent" | "`platform'" == "tencent" | "`platform'" == "TENCENT" {
	*默认搜索单曲
	if "`kind'" == "" {
	local kind = "0"
	} 
	//0,单曲；8,专辑；12,MV；7,歌词；9,歌手图片；
	
	*获取songmid等各种信息
	local URL1="http://c.y.qq.com/soso/fcgi-bin/client_search_cp?p=1&n=1&w="
	local URL2="&format=json&t="
	local URL="`URL1'`songname'`URL2'`kind'" //构造获取歌曲信息的URL
	clear	
	jsonio kv, file(`URL')
	qui keep if ustrregexm(key, "/data/song/list_1/singer/id/name") == 1 | ustrregexm(key, "/data/song/list_1/songname") == 1 | ustrregexm(key, "/data/song/list_1/songmid") == 1
	local ID=value[3]     //歌曲ID
	local song_name=value[4] //歌曲中文名
	local singer=value[1] //歌手名字
	
	//获取歌词
	clear
	local URL1="http://c.y.qq.com/soso/fcgi-bin/client_search_cp?p=1&n=1&w="
	local URL2="&format=json&t=7"
	local URL="`URL1'`songname'`URL2'" //构造获取歌词的URL
	jsonio kv, file(`URL')
	//如果是纯音乐：
	qui keep if ustrregexm(key, "/data/lyric/list_1/content") == 1
	if ustrregexm(value, "没有填词") == 1 {
	dis as txt char(10) "This is a pure music without lyrics, please enjoy."
	}
	//如果不是纯音乐，截取歌词
	else {
	qui drop key
	qui split value, parse("\n")
	qui sxpose, clear //用了外部命令sxpose
	qui replace _var1=subinstr(_var1,char(10),"",.) //删除换行符
	qui keep if _var1!=" " | _var1!= "" | _var1!= "char(9)"
	qui duplicates tag, generate(dup)
	qui drop if dup==0 | dup==1
	qui duplicates drop
	qui drop dup
	//打印在屏幕上
	local size = _N
	forvalues i=1/`size'{
	dis as txt _var1[`i']
	}
	}
	
	*获取播放地址	
	local URL1="http://u.y.qq.com/cgi-bin/musicu.fcg?format=json&data=%7B%22req_0%22%3A%7B%22module%22%3A%22vkey.GetVkeyServer%22%2C%22method%22%3A%22CgiGetVkey%22%2C%22param%22%3A%7B%22guid%22%3A%22358840384%22%2C%22songmid%22%3A%5B%22"
	local URL2="%22%5D%2C%22songtype%22%3A%5B0%5D%2C%22uin%22%3A%221443481947%22%2C%22loginflag%22%3A1%2C%22platform%22%3A%2220%22%7D%7D%2C%22comm%22%3A%7B%22uin%22%3A%2218585073516%22%2C%22format%22%3A%22json%22%2C%22ct%22%3A24%2C%22cv%22%3A0%7D%7D"
	local URL="`URL1'`ID'`URL2'" //构造获取歌曲信息的URL
	clear	
	jsonio kv, file(`URL')
	
	*构造最终播放地址
	qui keep if ustrregexm(key, "purl") == 1 | ustrregexm(key, "sip") == 1
	local URL1=value[2]     //歌曲ID
	local URL2=value[1] //歌曲中文名
	local URL="`URL1'`URL2'"
	
	}
	
	
	}
	
	}
	

	
	//打印在屏幕上：歌曲的信息和链接地址
	*dis as txt"<<" "`song_name'" ">>, by" "`singer'"
	dis as txt   "{bf: <<`song_name'>>, by `singer'}"
	dis as txt	"`description'"
	dis as txt  `" {browse "`URL'":`URL'}	
	
	//browse选项自动打开网址
	if "`browse'" != "" {
	view browse "`URL'" //网易云就是网易云音乐的界面，QQ只有一个音频播放页
	}
	
restore
end

cap prog drop lyrics
prog def lyrics

	syntax anything, ID(string)

	//获取歌词
	local URL1="http://music.163.com/api/song/media?id="
	local URL="`URL1'`id'" //构造获取歌词的URL
	clear
	jsonio kv, file(`URL')
	//如果是纯音乐：
	if ustrregexm(key, "/nolyric_1") == 1 & ustrregexm(value, "true") == 1 {
	dis as txt char(10) "This is a pure music without lyrics, please enjoy."
	}
	//如果不是纯音乐，截取歌词
	else {
	qui keep if ustrregexm(key, "/lyric_1") == 1
	qui drop key
	qui split value, parse("[" "]")
	qui sxpose, clear //用了外部命令sxpose
	qui replace _var1=subinstr(_var1,char(10),"",.) //删除换行符
	qui keep if _var1!=" " | _var1!= "" | _var1!= "char(9)"
	qui duplicates tag, generate(dup)
	qui drop if dup==0
	qui duplicates drop
	qui drop dup
	//打印在屏幕上
	local size = _N
	forvalues i=1/`size'{
	dis as txt _var1[`i']
	}
	}
end