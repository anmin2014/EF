

# 多期多个体倍分法 DID 模型

&emsp;

**作者**：宋燕欣  ( 中山大学 )    
**E-Mail:**  <songyx6@mail2.sysu.edu.cn>

[toc]

## 1. 背景介绍

​		标准的双边固定效应 DID 模型研究的是两组样本、在两个时期下、二元处理的事件效果，而 DID 多期多个体倍分法 ( DIDM ) 在标准 DID 模型下进行扩展，研究多组样本、多个时间段、非二元处理的事件效果。本文介绍 Stata 中 `did_multiplegt` 命令实现 DIDM 模型估计。



## 2. 模型介绍

### 2.1 传统DID模型

​    	在双向固定效应基础下，传统 DID 模型加入了两个虚拟变量，分别是处理组变量 $treat$ ( 处理组为1，控制组为0 ) 和处理期变量 $post$ ( 受到处理的时间点为1，其他时间为0 )。

​		模型设定为：

$$
y_{i,t} = \alpha + \mu_{i}+\lambda_{t}+\theta \times treat_{i} \times post_{t}+\beta \times x_{i,t}+\epsilon_{i,t} \quad (1)
$$
​		其中，**y** 是因变量，$\mu$ 代表个体的固定效应，$\lambda$ 代表时间的固定效应，**x** 表示的是随时间和个体变化的控制变量，$\theta$ 和 $\beta$ 分别是估计的变量，$\epsilon$ 为误差项。



### 2.2 多期DID模型

​		传统 DID 模型中只有两期的数据，假设处理组中所有的样本受到处理是同时的，这个设定在实际的研究中有较大的限制，例如不同地区的政策出台时间不同，传统的 DID 模型就不适用了。多期的 DID 模型解决了处理时间点不同的问题，模型设定为：

$$
y_{i,t} = \alpha + \mu_{i}+\lambda_{t}+\theta \times treat_{i} \times post_{i,t}+\beta \times x_{i,t}+\epsilon_{i,t} \quad (2)
$$
​		与传统 DID 模型不同的只有变量 **post**，传统 DID 只受时间 t 的影响，在多期 DID 中，则有个体 i 和时间 t，**treat** 变量仍然是二元的。



### 2.3 多期多个体DIDM模型 

​		因为双边固定效应多期 DID 模型假设对于样本的处理是不变的，这样的假设在现实中常常不成立，处理变量可能不是一个二元变量，比如不同地区的政策力度不同；而处理也并非是一次性的，样本不能简单地一分为二处理组和控制组，比如降雨对于地区的影响，会出现从有到无、从无到有的多个交替过程，需要一个动态的处理组。如以下的数据，**nr** 代表个人 ID，150 号个体在 1987 年加入了工会，而 166 号个体在 1980-1984 年均为工会成员，但在 1985 年后退出了工会：

```
 nr    year      union
 
 150   1980       0 
 150   1981       0 
 150   1982       0 
 150   1983       0 
 150   1984       0 
 150   1985       0 
 150   1986       0 
 150   1987       1 
....................
 166   1980       1 
 166   1981       1 
 166   1982       1 
 166   1983       1 
 166   1984       1 
 166   1985       0 
 166   1986       0 
 166   1987       0 

```

​		因此有动态处理组设定，放松了传统 DID 模型中处理不变的设定，解决了上述的问题。详细内容将在下文介绍。在模型中 **D** 表示处理，可以为二元变量，也可以是非二元，文中以二元变量为例。



**固定效应 ( Fixed Effect )**

​		固定效应是设定组间固定效应和时间固定效应的OLS回归，有：
$$
D_{g,t} = \alpha + \gamma_{g}+\lambda_{t}+\epsilon_{g,t} \quad (3)
$$
​		其中，
$$
D_{g,t}=\dfrac{1}{N_{g,t}}\sum ^{N_{g,t}}_{i=1}D_{i,g,t},  Y_{g, t}(0)=\frac{1}{N_{g, t}} \sum_{i=1}^{N_{g, t}} Y_{i, g, t}(0),  Y_{g, t}(1)=\frac{1}{N_{g, t}} \sum_{i=1}^{N_{g, t}} Y_{i, g, t}(1) \quad (4)
$$
​		$D_{g, t}$ 代表在  g 组，t 时的平均处理量;  $Y_{g, t}(0)$ 代表不受政策处理的组平均结果; $Y_{g, t}(1)$ 则是受到政策处理的组平均结果。
$$
\Delta_{g, t}=E\left(\frac{1}{N_{g, t}} \sum_{i=1}^{N_{g, t}}\left(Y_{i, g, t}(1)-Y_{i, g, t}(0)\right)\right) \quad (5)
$$

$$
w_{g, t}=\frac{\varepsilon_{g, t}}{\sum_{(g, t): D_{g, t}=1} \frac{N_{g, t}}{N_{1}} \varepsilon_{g, t}}\quad (6)
$$


$$
\beta_{fe}= \sum_{(g,t):D_{g,t}=1}W_{g,t}\Delta_{g,t} \quad (7)
$$
​		固定效应的系数估计也可以理解为 ATE ( Average Treatment Effects ) 的加权平均，它的估计可以由两步得到。首先第一步是个体固定效应、时间固定效应对于处理变量 **D** 的回归 ( 3 )，可以理解为去掉了政策效果中时间和个体的特征，得到权重 **W** ( 6 ) ；第二步得到 $\beta_{fe}$ 的估计  ( 7 )。模型假定在 t 和 t-1 都无政策的组有共同趋势。

​		注意到估计中的权重里，允许权重为负数，这是因为在模型中，“控制组” 有时是所有时间段中都没有受到政策影响的样本，有时则是指两个时间点都受到处理的组，这两种更贴切的名称是稳定组 ( stable ) 。那么对于两个时间点都受到处理的组，在第二个时间点上的权重是负数，但如果负权重太大，估计值是否会出现负数呢？尤其是模型中的时间、组数太多的情形，出现较大负权重的概率更大，最终的固定效应估计值就会出现负数。作者列举了一个简单的例子：

​		设定两组实验组，在 t = 0 时，两组都不受处理；在 t = 1 时，A 组受处理，B 组不受处理；在 t = 2 时，两组都受到处理。这时，权重分别是 A 组在两期的权重分别是 1/3，-1/6，B组在第二期的权重为 1/6。双边固定效应的表达式是 $\beta_{f e}=1 / 2 \Delta_{0,2}+\Delta_{1,1}-1 / 2 \Delta_{1,2}$，如果 $\Delta_{1,2}$ 特别大，就会出现负数结果，如 $\beta_{f e} = 1/2 \times 1 +1-1/2 \times4 = -1/2 $ 。

​		明明政策是正向的，政策效果都是正面的，但是估计出现负数。这是因为，在异质性问题上，不同组的共同趋势不同，该模型的负数权重可能带来偏误，如果一个政策效果是正的，太多负数的权重可能会导致系数估计结果为负。



**异质性检验**

​		文章提供了两个检验异质性处理效应稳健性的方法：第一个是 $\underline{\sigma}_{f e}$，系数估计除以权重的标准差，如果接近 0，说明双边固定效应是不稳健的，容易导致系数估计的符号与实际相反：
$$
\underline{\sigma}_{f e}=\frac{\left|\beta_{f e}\right|}{\sigma(\boldsymbol{w})}\quad (8)
$$

$$
\begin{aligned}
\sigma(\boldsymbol{w}) &=\left(\sum_{(g, t): D_{g, t}=1} \frac{N_{g, t}}{N_{1}}\left(w_{g, t}-1\right)^{2}\right)^{1 / 2} \quad (9)
\end{aligned}
$$

​		第二个是 $\underline{\underline{\sigma}}_{f e}$ 只考虑有权重为负数的情况，检验系数估计接近 0，说明双边固定效应是不稳健的。如果是 $\Delta_{g,t} s$ 比较小，$\underline{\sigma}_{f e}$ 的结果很大，但第二个检验仍能接近于 0,能更好地检验异质性。

$$
\begin{aligned}
\underline{\underline{\sigma}}_{f e} &=\frac{\left|\beta_{f e}\right|}{\left[T_{s}+S_{s}^{2} /\left(1-P_{s}\right)\right]^{1 / 2}} (s=\min \{i \in\{1, \ldots, n\}: w_{(i)}<-S_{(i)} /(1-P_{(i)}) \quad (10)
\end{aligned}
$$

​		对于这个问题，提出了下文的估计方法：



**转换效应 ( Switching Effect )**

​		为了解决上述的异质性问题，文章提出了新的估计量，考虑到了政策处理从无到有、从有到无两个正负方向的效果，进行加权平均，而两期的处理不变 ( 如：两期都是1或两期都是0 ) 的样本，则作为控制组。对比多期 DID 模型，仅考虑了政策从无到有的情况。
$$
W_{T C}=\sum_{t=1}^{\bar{t}}\left(\frac{N_{1,0, t}}{N_{S}} D I D_{+, t}+\frac{N_{0,1, t}}{N_{S}} D I D_{-, t}\right) \quad (11)
$$
​		$D I D_{+, t}$ 指的是政策从 0 到 1 的平均结果减去稳定组（两期都是0）的平均结果；$D I D_{-, t}$ 指的是稳定组 ( 两期都是1 ) 的平均结果减去政策从 0 到 1 的平均结果。
$$
\begin{array}{l}
D I D_{+, t}=\sum_{g: D_{g, t}=1, D_{g, t-1}=0} \frac{N_{g, t}}{N_{1,0, t}}\left(E\left(Y_{g, t}\right)-E\left(Y_{g, t-1}\right)\right)-\sum_{g: D_{g, t}=D_{g, t-1}=0} \frac{N_{g, t}}{N_{0,0, t}}\left(E\left(Y_{g, t}\right)-E\left(Y_{g, t-1}\right)\right) \quad (12)\\
D I D_{-, t}=\sum_{g: D_{g, t}=D_{g, t-1}=1} \frac{N_{g, t}}{N_{1,1, t}}\left(E\left(Y_{g, t}\right)-E\left(Y_{g, t-1}\right)\right)-\sum_{g: D_{g, t}=0, D_{g, t-1}=1} \frac{N_{g, t}}{N_{0,1, t}}\left(E\left(Y_{g, t}\right)-E\left(Y_{g, t-1}\right)\right) \quad (13)
\end{array}
$$
​		**假定共同趋势**：稳定组组内共同趋势，即对于在 t 和 t-1 都无政策的组有共同趋势，而在 t 和 t-1 都有政策的组有共同趋势。这个设定比上面的设定要宽松，固定效应的模型要求不同组的共同趋势也相同。



**安慰剂效应 ( Placebo Test )**

​		在 DID 模型估计中，常常用到安慰剂检验，证明政策的有效性。在 t-1 时期实施安慰剂检验，在 t-1 期模拟 t 期的政策，对比 t-1 和 t-2 的因变量。在 DIDM 模型中，对于 t 期政策为 1 的，找出 t-1 和 t-2 期政策都是 0 的样本，对比三期都是 0 的样本，比较两组的平均结果之差，是 $D I D_{+, t}^{p l}$；另外，对于  t 期政策为 0 的，找出  t-1 和  t-2 政策都是 1 的样本，对于三期政策都为 1 的样本，对比平均结果的差别，是 $D I D_{-, t}^{p l}$。因为 t-1 和 t-2 期实际上没有政策的变化，所以如果模型的设定符合，DID 应该接近于 0，估计值显著为 0。
​    有安慰剂 Wald-TC 估计量表达式：
$$
W_{T C}^{p l}=\sum_{t=2}^{\bar{t}}\left(\frac{N_{1,0,0, t}}{N_{S}^{p l}} D I D_{+, t}^{p l}+\frac{N_{0,1,1, t}}{N_{S}^{p l}} D I D_{-, t}^{p l}\right) \quad (14)
$$
​		



## 3. 模型估计及Stata使用

### 3.1 命令介绍

​		在Stata `twowayfeweights` , `fuzzydid` 或 `did_multiplegt` 命令，`fuzzydid` 或 `did_multiplegt` 适用于权重出现负数，而且比例不特别大的情况，`fuzzydid` 适用于模糊处理 ( fuzzy designs ) 处理组出现随机 ( stochastic ) 的情况，而本文介绍的 `did_multiplegt` 用于处理清晰变化 ( sharp designs ) ，处理组非随机 ( non-stochastic ) 。

```
did_multiplegt Y G T D [if] [in] [, options]
```

​		在变量输入中，`Y` 输入结果变量，`G` 输入分组变量，`T` 是时间变量，`D` 是处理变量。

​		`options` 选项中，`placebo(#)` 输入安慰剂效应的数量；`controls(varlist)` 中加入控制变量；`breps(#)` 中输入重抽样（bootstrap）的次数，计算标准误，更多的选项请阅读命令 help 文件。

​		报告的结果保存到 `e()` 中，分别有 ` e(effect_0)` 估计政策转换的效应；`e(N_effect_0)` 用于估计的样本数；而 `e(N_switchers_effect_0)` 是估计中出现政策转换 ( 0到1、1到0 ) 的数量；` e(se_effect_0` 是当启用`breps()`时报告标准误；启用安慰剂检验选项后，` e(placebo_i)` 是在政策转换之前 **i** 期的安慰剂效应估计；`e(N_placebo_i)` 是估计安慰剂用到的样本数；`e(se_placebo_i)` 是安慰剂效应的标准误。更多的结果详见 help 文件。



### 3.2 论文示例

​		Vella and Verbeek ( 1998 ) 是一篇发表在 Journal of Applied Econometrics 的论文，研究年轻男性是否加入工会对于他们的工资增长的影响，论文研究结果表明工会工人的平均工资增长相比于非工会工人高20%左右。在本文中，我们使用这篇论文相同的面板数据估计工会对于工资的影响，首先使用 `twowayfeweights` 估计双边固定效应的系数，检验异质处理的稳健性，再使用 `did_multiplegt` 命令估计状态转换 ( switchers ) 的平均处理效应。

```
//下载数据
ssc install bcuse
bcuse wagepan

//数据的基本统计情况如下

Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
       lwage |      4,360    1.649147    .5326094  -3.579079    4.05186
          nr |      4,360    5262.059     3496.15         13      12548
        year |      4,360      1983.5    2.291551       1980       1987
       union |      4,360    .2440367    .4295639          0          1

```

​		在该模型中用到的结果变量 `lwage` 是工资对数，分组变量 `nr` 相当于每一个人的ID， 时间变量 `year` 是1980-1987 年 8 年之间的数据，处理变量 `union` 是二元变量，1 表示属于工会，0 表示不属于工会的工人。

```
//固定效应估计和检验
twowayfeweights lwage nr year union, type(feTR)
```

​		结果显示：

```
Under the common trends assumption, beta estimates a weighted sum of 1064 ATTs. 
860 ATTs receive a positive weight, and 204 receive a negative weight.
The sum of the positive weights is equal to 1.0054686.
The sum of the negative weights is equal to -.00546854.
beta is compatible with a DGP where the average of those ATTs is equal to 0,
while their standard deviation is equal to .09357625.
beta is compatible with a DGP where those ATTs all are of a different sign than beta,
while their standard deviation is equal to 5.389153.
```

​		固定效应估计中，有 860 个正权重，204 个负权重，两个检验发现系数估计在异质处理下接近于 0，是不稳健的，因此我们使用 DIDM模型 `did_multiplegt` 进一步观察工会对于工人工资的影响。



```
//模型估计
did_multiplegt lwage nr year union, placebo(1) breps(50) cluster(nr)

//显示结果
ereturn list
```

​		选项设定了一次安慰剂实验，按照 `nr` 进行 50 次重抽样，结果显示：

```
scalars:
  e(effect_0) =  .0261225951945861
  e(se_effect_0) =  .0194752588662298
  e(N_effect_0) =  3815
  e(N_switchers_effect_0) =  508
  e(placebo_1) =  .099321110943454
  e(se_placebo_1) =  .0315586535625052
  e(N_placebo_1) =  2842
```



​		当命令启用了重抽样，结果会返回一张图表，包含估计值、安慰剂效应以及其95%置信区间的结果。如下图所示：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/宋燕欣_多期多个体倍分法DID模型_Fig01.png)



​		结果显示，工会的作用效果 0.026，加入工会能够提高工资 2.6%，此结果与原论文的估计20%差异较大，说明因为处理的异质性确实影响了系数估计的结果；在安慰剂效应中估计为 0.099，拒接安慰剂估计为 0 的假设，说明模型可能不符合共同趋势的设定。再来看时间处理前后 5 期的效果，在命令的选项中加上 `dynamic(5),placebo(5)`，结果如下：

```
did_multiplegt lwage nr year union, placebo(5) breps(50) cluster(nr) dynamic(5)
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/宋燕欣_多期多个体倍分法DID模型_Fig02.png)

​        如上图所示，加入工会的作用在 0-3 期的作用持续上升，到第 4 期回到 0 的水平上。而在 t = 0 之前的安慰剂检验则波动较大，再次说明模型可能不符合共同趋势的设定。



## 4. 总结

​		双边固定效应 DID 模型是实证研究的重要模型之一，但文章发现在组别和时间上出现较大的异质性时，双边固定效应的 DID 估计可能并不稳健，特别是出现负权重时，将影响估计的正负性，导致研究的结论出现相反的结果。因此，文章提出了两个检验异质性的方法，可以在 Stata 中使用 `twowayfeweights` 检验；并且提出了多期多个体 DIDM 模型，该模型放松了部分共同趋势假定的约束，在异质性的情况下，估计更加稳健。可以在 Stata 中使用 `did_multiplegt` 命令计算 DIDM 模型的估计。



## 5. 参考文献

- de Chaisemartin, Clément, and Xavier D'Haultfœuille. 2020. Two-Way Fixed Effects Estimators with Heterogeneous Treatment Effects [J]. American Economic Review, 2020, 110 (9): 2964-96. [-PDF-](https://www.aeaweb.org/articles?id=10.1257/aer.20181169)
- Vella, F. and Verbeek, M.  Whose wages do unions raise? a dynamic model of unionism and wage rate determination for young men.[J].  Journal of Applied Econometrics, 1998,  13(2), 163–183. [-PDF-](www.jstor.org/stable/223257. )

- 张远远, 连享会推文, [Stata: 多期倍分法 (DID) 详解及其图示](https://zhuanlan.zhihu.com/p/102428197)