**写作目录如下：（正文初稿已提交）**

**·** 1. 背景介绍<br/>
**·** 2. RIF是什么？<br/>
**·** 3. RIF回归：什么导致了收入分配不平等？<br/>
&nbsp;&nbsp;&nbsp;&nbsp;**·** 3.1 标准RIF回归<br/>
&nbsp;&nbsp;&nbsp;&nbsp;**·** 3.2 标准RIF回归：操作实例<br/>
&nbsp;&nbsp;&nbsp;&nbsp;**·** 3.3 RIF回归：分组处理效应<br/>
&nbsp;&nbsp;&nbsp;&nbsp;**·** 3.4 RIF回归：分组处理效应操作实例<br/>
**·** 4. 进阶拓展：RIF分解<br/>
&nbsp;&nbsp;&nbsp;&nbsp;**·** 4.1 RIF分解介绍<br/>
&nbsp;&nbsp;&nbsp;&nbsp;**·** 4.2 RIF分解：操作实例<br/>
**·** 5. 注意事项