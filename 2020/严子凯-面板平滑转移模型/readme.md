## 项目简介
PSTR — Panel Smooth Transition Regression Modelling.

- González, Andrés, Timo Teräsvirta, Dick van Dijk, and Yukai Yang. 2017. “Panel Smooth Transition Regression Models.” CREATES Research Papers. [-PDF-](https://swopec.hhs.se/hastef/papers/hastef0604.pdf)
- Gonzalez, Andres, Timo Teräsvirta, and Dick van Dijk. 2005. Panel Smooth Transition Regression Models. Research Paper Series. [-PDF-]()
- [微软学术 - Appliations of PSTR](https://academic.microsoft.com/search?q=Panel%20Smooth%20Transition%20Regression%20Modelling&f=&orderBy=0&skip=0&take=10)
> Panel Smooth Transition Regression Models
Andr´es Gonz´aleza
, Timo Ter¨asvirtabc, Dick van Dijk∗def
, and Yukai Yangg
## 参考项目
-  Homepage of R package: <https://github.com/yukai-yang/PSTR>
  - [PSTR-码云镜像](https://gitee.com/arlionn/PSTR-1)
  - [PSTR-码云镜像-v2](https://gitee.com/arlionn/PSTR)