# 提交说明
https://gitee.com/arlionn/EF/blob/master/2020/readme.md
# readme.md
https://gitee.com/arlionn/EF/blob/master/README.md
# FILES
https://gitee.com/arlionn/EF/tree/master/2020
# ZIP TOPIC
https://gitee.com/Stata002/StataSX2018/wikis/zip%E6%A8%A1%E5%9E%8B%E4%BB%8B%E7%BB%8D.md?sort_id=1435801
# TARGET
https://stats.idre.ucla.edu/stata/dae/zero-inflated-poisson-regression/
# REFERENCES
1.基于ZIP模型的零膨胀检验方法的比较研究

2.基于零膨胀模型的高速公路事故形态影响因素分析

3.零膨胀广义泊松模型的推广及其在精算中应用*
# STATA
```{stata}
[R] zip -- Zero-inflated Poisson regression
           (View complete PDF manual entry)


Syntax

        zip depvar [indepvars] [if] [in] [weight],
               inflate(varlist[, offset(varname)]|_cons) [options]
```
