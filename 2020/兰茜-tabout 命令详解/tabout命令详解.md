&emsp;

\> **作者**：兰茜 (中山大学)

\> **E-Mail:** [lanx6@mail2.sysu.edu.cn]()

# tabout 命令详解

[TOC]

## 1. 引言

一个好的表格应该是怎样的？

Edward Tufte (2001) 在讨论这个问题时，提出了两个制作表格的重要原则：1. 在有限空间内展示大量的数字和信息；2. 能够用肉眼去比较和判断数据结果。

Ian Watson (2015) 则提出，”出版物级别“ 的表格，至少应该是内容丰富并且美观的。

我们通过一些文献中的表格，能够更直观的体会表格之美。

> 范例 1：封进, 艾静怡, 刘芳. 退休年龄制度的代际影响——基于子代生育时间选择的研究[J]. 经济研究, 2020, 55(09): 106-121.

![范例 1](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/兰茜_tabout详解_Fig11.png)

> 范例 2：程海艳, 李明辉, 王宇. 党组织参与治理对国有上市公司盈余管理的影响[J]. 中国经济问题, 2020(02): 45-62.

![范例 2](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/兰茜_tabout详解_Fig13.png)

> 范例 3：ARGYLE BRONSON, NADAULD TAYLOR, PALMER CHRISTOPHER, et al. The Capitalization of Consumer Financing into Durable Goods Prices. The Journal of Finance, 2020, 76(1): 169-210. 

![范例 3](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/兰茜_tabout详解_Fig10.png)

在了解什么是好的表格之后，另一个问题出现了：能否方便快捷地生成一个卓越的表格，尤其是出版物级别的表格呢？

传统的 Stata 制表命令基本无法直接生成适合列入出版物中的表格，Stata 用户往往需要进一步加工才能使表格满足出版物的要求。

多年来，许多 Stata 用户编写了命令程序来解决这一问题，Ben Jann 编写的 `estout` 命令便是其中最好的命令之一。 然而，`estout` 命令专注于对估计结果的输出，与之对应，专注于描述性统计结果输出的 `tabout` 命令应运而生。

接下来，就让我们一起来揭开 `tabout` 命令的神秘面纱吧！



## 2. tabout 命令简介

### 2.1 tabout 的不同版本

`tabout` 命令的作者是 **Ian Watson**，其功能十分强大，能够帮助 Stata 用户高效地生成**出版物级别**的高质量**描述性统计表格**。目前 `tabout` 命令已更新到 3.0.9 beta 版本，但 Version 3 仍为测试版本，最新正式版本为 2.0.8 版本。



Version 3 beta 主要新增了以下功能：

- **新的导出格式：_Word_ 和 _Excel_**
- **新增表格标题和脚注选项**
- 支持删除不需要的行或列和重新构造表格
- 模板文件更易于使用
- 能够轻松创建动态文档



`tabout` 属于 Stata 的外部命令，其安装方式如下：

- Version 2 安装方式： `ssc install tabout, replace`
- Version 3 beta 安装方式：在 [tabout home page](http://tabout.net.au/docs/home.php) 可获取最新版本，下载后可以将其放置在 Stata 工作目录中，或放在 /ado/Personal/ 目录中。
- 想要知道目前使用 `tabout` 版本，在 Stata 命令窗口输入 `which tabout` 即可。



由于 Version 3 功能更强大并且支持导出到 Word 和 Excel，本文基于 Version 3 对 `tabout` 命令进行介绍，使用的是 3.0.9 beta 版本。



### 2.2 tabout 的语法结构

`tabout` 命令语法结构如下：

```stata
tabout varlist [if exp] [in rang] [wright = exp] using filename [,options]
```

**基本说明：**

- `varlist` 为变量列表，`tabout` 自动将先输入变量设定为行分类变量，最后一个变量设定为列分类变量。若指定了 `oneway` 选项，则所有变量都被设定为列分类变量。

- `using filename` 用于指定导出表格的文件名称和类型，如"table1.docx"。



**options 可选项说明：**

`options` 可替换的内容较多，接下来将分类对一些选项进行介绍。



**核心选项**

- `REPlace` ：替换现有的输出文件。
- `APPend` ：将表格保存到现有文件中。
- `ONEway` ：将所有变量设定为列分类变量。
- `sum` ：制作 summary table 时必须添加的选项。
- `style()` ：表格输出样式，包括 `tex htm xlsx xls docx tab csv semi` 。
- `Contents()` ：指定表格输出的内容，**默认为 `freq`** 。在 Version 2 中，该选项称为 `Cell()` 。
- `show()` ：设定在 Stata 命令窗口中显示的内容，包括 `none all output prepost comp` ，默认为 `output`。
- `sort ` ：对表格里的值进行排序。



**表格布局的相关选项**

- `TOTal()` ：添加总计的标签。

- `NOHLines` ： No heading lines，没有标题线。

- `NOPLines` ：No panel lines，不同面板数据之间不加横线。

- `NOBORDer` ：No table borders，表格顶部和底部不加边界线。

- `LAYout()` ：列和行的布局，包括 `col row CBlock RBlock` 。

- `TWidth()` ：Table width，设置表格宽度，Version 3 新增选项。

- `CWidth()` ：Column width，设置列宽，Version 3 新增选项。

- `LWidth()` ：Label width，设置标签行 (第一行) 的宽度，Version 3 新增选项。

  

**表格内容的输出格式的相关选项**

- `Format()` ：数据的显示格式。
- `FAMily()` ：使用的字体，Version 3 新增选项。
- `fsize()` ：字体大小，默认为 10pt ，Version 3 新增选项。
- `font()` ：指定标题和变量标签的字体样式，包括 `bold italic none`。
- `hright` ：标题行右对齐。`tabout` 默认的对齐方式为居中对齐，Version 3 新增选项。
- `MONey()` ：指定货币符号，和 `format` 一起使用。例如：`format(2m) money(£)`。



**行标题与列标题的相关选项**

- `h1()` ：Heading row 1，设定标题行 1 的文本内容，`h2()` ，`h3()` 以此类推。
- `h1c()` ： Heading row 1 columns，设定标题行 1 的列宽，`h2c()` ，`h3c()` 以此类推，均为 Version 3 新增选项。

- `clab()` ：column label，设定表格第三行的列头，不同列的列头要用空格隔开，列头文本中的空格要用下划线代替。

  

**表的标题和脚注的相关选项**

- `fn()` ：添加脚注，Version 3 新增选项。

- `title()` ：添加表格标题，Version 3 新增选项。

  

**文件导出的相关选项**

- `tp()` ：template file，模版文件，Version 3 新增选项。
- `open` ：在对应的应用程序中打开导出的表格文档， Version 3 新增选项。



**重塑表格的相关选项**

- `plugc()` ：添加列，Version 3 新增选项。
- `plugr()` ：添加行，Version 3 新增选项。
- `dropc()` ：去掉不必要的列，Version 3 新增选项。

- `dropr() ` ：去掉不必要的行，Version 3 新增选项。
- `PLUGSYMbol()` ： 缺失值符号，如 `plugsym(N/A)` ：用 N/A 表示缺失值，Version 3 新增选项。



**样本容量 N 的相关选项：**

- `nlab()`  ：N 的标签，如 ” Sample size “。
- `npos()` ：N 的位置，包括 `col row lab tufte` 。

- `pop` ：显示总体数，而不是样本数。



**统计检验的相关选项：**

- `stats()` ：显示不同类型的统计分析值，包括 `chi2 gamma V taub lrchi2`。

- `stlab()` ：统计值的标签，Version 3 新增选项。

- `stform()` ：统计值的显示格式，默认保留 3 位小数，Version 3 新增选项。

- `stars` ：p 值星号的显示，Version 3 新增选项。

- `plab()` ：p 值的标签，Version 3 新增选项。

- `pform()` ：p 值的显示格式，默认保留 3 位小数，Version 3 新增选项。



**诊断性检验的相关选项：**

- `mi` ：missing，显示缺失值。

- `wide` ：Mata 矩阵的宽度。

  

## 3. tabout 命令制作基本表 

在了解了 `tabout` 的语法结构后，让我们用一些范例来更深入地了解如何使用 `tabout` 制作出各种精致的表格吧！

这一节将用范例向大家介绍如何用 `tabout` 命令制作基本表 (basic bable) 并导出。



### 3.1 二维基本表的制作与导出

#### 3.1.1 制作简单二维基本表的并导出到 Excel

```stata
// 数据处理
sysuse cancer, clear
la var died "Patient died"
la de ny 0 "No" 1 "Yes", modify
la val died ny

la var drug "Drug type"
la def drug 1 "Placebo" 2 "Trial drug 1" 3 "Trial drug 2", modify
la val drug drug

// 表格制作并导出
tabout died drug using Table1.xls, ///
replace c(freq col cum)  ///
clab(No. Col_% Cum_%) f(0 1) ///
title(Table1: Simple Example of Twoway Basic Table) ///
fn(Source:cancer.dta)
```

其中：

- `tabout died drug` ：`die` 为列分类变量，`drug` 为行分类变量。
- `using Table1.xls` ：导出名为 “Table 1“ 的 xls 文件。
- `c(freq col cum)` ：指定输出内容为频数、列百分比和累计百分比。在制作普通基本表时，`Content()` 中可以输入 `freq cell row col cum` 中任意数量的选项。
- `clab(No. Col_% Cum_%)` ：第三行的列头，频数列的列头为 NO. ，行百分比列的列头为 Col % ，累计百分比列的列头为 Cum % 。
- `f(0 1)` ：设定输出数据保留的小数点位数。
- `title(Table 1: A Simple Example of Twoway Basic Table)` ：添加标题。
- `fn(Source:cancer.dta)` ：添加脚注。



Stata 输出结果如下：

```stata
Table 1: Simple example of twoway basic table
        Drug type
                        Placebo                 Trial drug 1
>     Trial drug 2                    Total
        No.     Col %   Cum %   No.     Col %   Cum %   No.     Col %   Cum
> %   No.     Col %   Cum %
Patient died
No      1       5.0     5.0     8       57.1    57.1    8       57.1    57.1
>     17      35.4    35.4
Yes     19      95.0    100.0   6       42.9    100.0   6       42.9    100.
> 0   31      64.6    100.0
Total   20      100.0           14      100.0           14      100.0
>     48      100.0
Source:cancer.dta
```



Excel 结果如下图：

![Table 1: Simple example of twoway basic table](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/兰茜_tabout详解_Fig01.png)



#### 3.1.2 制作面板数据的交叉表并导出到 Excel

```stata
// 数据处理
sysuse nlsw88, clear
la var south "Location"
la def south 0 "Does not live in the South" 1 "Lives in the South", modify
la val south south

la var race "Race"
la def race 1 "White" 2 "Black" 3 "Other", modify
la val race race

la var collgrad "Education"
la def collgrad 0 "Not college graduate" 1 "College graduate", modify
la val collgrad collgrad

// 表格制作并导出
tabout south race collgrad using Table2.xlsx, ///
replace style(xlsx) c(freq row col) ///
f(0c 1 1) font(bold) layout(rb) clab(_ _ _) h3(nil)  ///
title(Table 2: Example of cross tabulation using panels) ///
fn(Source: nlsw88.dta)
```

其中：

- `h3(nil)` ：取消标题行 3。
- `font(bold)` ：标题和变量标签字体加粗。
- `clab(_ _ _)` ：各类输出数据的列头为空。
- `style(xlsx)` ：指定导出样式，加了这个选项之后，表格直接导出到 Excel 文档中， Stata 窗口不显示。
- `layout(rb)` ：表格以行块形式布局，即频数(freq)、行百分比(row)、列百分比(col) 放置在同一行而不是同一列中。



Excel 结果如下图：

![Table 2: Example of cross tabulation using panels](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/兰茜_tabout详解_Fig02.png)



#### 3.1.3 制作带有统计量的二维基本表并导出到 Word 

**以卡方检验为例，并将表格导出到 Word 。**

```stata
// 数据处理
sysuse nlsw88, clear
la var south "Location"
la def south 0 "Does not live in the South" 1 "Lives in the South", modify
la val south south

la var race "Race"
la def race 1 "White" 2 "Black" 3 "Other", modify
la val race race

la var collgrad "Education"
la def collgrad 0 "Not college graduate" 1 "College graduate", modify
la val collgrad collgrad

// 表格制作并导出
tabout south collgrad race using Table3.docx, replace ///
style(docx) c(col) clab(Col_%) twidth(40) ///
font(bold) f(1) npos(row) stats(chi2) ///
title(Table 3: Example of Chi-square test) fn(nlsw88.dta)
```

其中：

- `c(col)` ：指定输出内容为列百分比。 在制作带有统计量的基本表时，`Content()` 中可以输入 `freq cell row col cum` 中任意**一个**的选项。
- `npos(row)` ：N 的位置在行。
- `stats(chi2)` ：输出卡方检验的卡方值。
- `twidth(40)` ：指定导出的表格宽度为 40 cm。



Word 结果如下图：

![Table 3: Example of Chi-square test](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/兰茜_tabout详解_Fig03.png)



`tabout` 的一大特点是能够为用户提供定制化服务，通过改变一些选项，就能够对表格细节进行修改以达到用户需要的样式，下面就对上述卡方检验表进行修改。

```Stata
// 定制化表格
tabout south collgrad race using Table4.docx, replace ///
style(docx) c(col) clab(Col_%) font(bold) f(1) npos(row) ///
stats(chi2) stlab(Chi2) stlab(Chi2) stpos(col) stars ///
title(Table 4: Example of customisation of Chi-square test) fn(nlsw88.dta)
```

其中：

- `stlab(Chi2)` ：统计量的标签为 “ Chi2 ”。
- `stpos(col)` ：统计量的位置在列。
- `stars` ：P 值用星号表示。



Word 结果如下图：

![Table 4: Example of customisation of Chi-square test](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/兰茜_tabout详解_Fig04.png)



### 3.2 一维基本表的制作与导出

制作一维表格的关键就是加入 `oneway` 选项，范例如下：

```ststa
// 数据处理
sysuse cancer, clear
la var died "Patient died"
la de ny 0 "No" 1 "Yes", modify
la val died ny
la var drug "Drug type"
la def drug 1 "Placebo" 2 "Trial drug 1" ///
3 "Trial drug 2", modify
la val drug drug

// 表格制作
tabout died drug using Table5.xlsx,  ///
replace style(xlsx) font(bold) oneway c(freq col cum) ///
f(0c 1) clab(No. Col_% Cum_%) npos(col) ///
nlab(Sample) title(Table 5: Example of oneway table) ///
fn(Source:cancer.dta)
```

其中：

`oneway` ：告诉 `tabout` 所有的变量都为列变量

`nlab(Sample)` ：N 值的标签



Excel 结果如下图：

![Table 5: Example of oneway table](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/兰茜_tabout详解_Fig05.png)



## 4. tabout 命令制作汇总表

制作汇总表 (summary table)  的关键是 `sum` 选项，这是制作汇总表必须的选项，它告诉 `tabout` 表格中要输出的并不是频数相关数据，而是统计汇总数据，表格中具体输出内容仍用 `Content()` 选项指定。



### 4.1 二维汇总表的制作与导出

制作二维汇总表时，`Content()` 中可输入 `N count mean median var sd skewness kurtosis uwsum sum min max p1 p5 p10 p25 p50 p75 p90 p95 p99 iqr r9010 r9050 r7525 r1050` 中的任意**一个**选项，后跟对应的变量名称。

具体范例如下：

```ststa
sysuse auto, clear
tabout rep78 foreign using Table6.xlsx, ///
replace style(xlsx) font(italic) ///
c(mean weight) f(0c) sum h3(nil) ///
title(Table 6: Simple twoway summary table of means) ///
fn(auto.dta)
```

其中：

- `sum` ：制作 summary table 的必选项。
- `c(mean weight)` ：指定输出变量 **weight** 的平均值。
- `font(italic)` ：标题字体样式为斜体。



Excel 结果如下图：

![Table 6: Simple twoway summary table of means](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/兰茜_tabout详解_Fig06.png)



### 4.2 一维汇总表的制作与导出

制作一维汇总表时，`Content()` 中可输入 `N count mean median var sd skewness kurtosis uwsum sum min max p1 p5 p10 p25 p50 p75 p90 p95 p99 iqr r9010 r9050 r7525 r1050` 中的**任意数量**选项，每个选项后分别跟其对应的变量名称。

具体范例如下：

```ststa
sysuse auto, clear
tabout foreign rep78 using Table7.xlsx, ///
replace style(xlsx) font(bold)  ///
sum npos(tufte) hright ///
c(mean mpg mean weight mean length ///
median price median headroom) ///
f(1c 1c 1c 2cm 1c) h2(Mean Median) h2c(3 2) ///
clab(MPG Weight_(lbs) Length_(in) ///
Price Headroom_(in)) ///
title(Table 7: Oneway summary table ///
illustrating multiple summary measures) ///
fn(Source: auto.dta)
```

其中：

- `hright` ：标题行 3 右对齐。
- `npos(tufte)` ：将类别分布放置在列分类变量内。
- `h2(Mean Median)` ：标题行 2 的标题分别为 “ Mean ”、“ Median ”。
- `h2c(3 2)` ：指定标题行 2 的两个标题的宽度，第一个标题 Mean 跨越 3 行，第二个标题 Median 跨越 2 行。
- `c(mean mpg mean weight mean length median price median headroom)` ：分别输出 **mpg** 的平均值、 **weight** 的平均值、**length** 的平均值、**price** 的中位数、**headroom** 的中位数。



Excel 结果如下图：

![Table 7: Oneway summary table illustrating multiple summary measures](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/兰茜_tabout详解_Fig07.png)



关于 `tabout` 命令就介绍到这里啦，当然，还有很多功能如导出到 Latex、HTML 网页，表格模板和动态数据表格的制作等在文中未作详细介绍，大家可以通过 [tabout user guide](http://www.ianwatson.com.au/pubs/tabout_user_guide.pdf) 进一步探索哦！



## 参考资料

- Ian Watson, 2016, Publication quality tables in Stata: User Guide for **tabout** (Version 3). [-PDF-](http://www.ianwatson.com.au/pubs/tabout_user_guide.pdf) 
- Ian Watson, 2015, Publication quality tables in Stata: a tutorial for the **tabout** program (Version 2).  [-PDF-](http://www.ianwatson.com.au/stata/tabout_tutorial.pdf)
- 封进, 艾静怡, 刘芳. 退休年龄制度的代际影响——基于子代生育时间选择的研究[J]. 经济研究, 2020, 55(09): 106-121. [-CNKI-](https://kns.cnki.net/kcms/detail/detail.aspx?dbcode=CJFD&dbname=CJFDLAST2020&filename=JJYJ202009008&v=x7X5dmdiedMHOl4Vb5g%25mmd2F5PwoRRc2MOKTJMdrOwthKfGaPuUXXlMPjmSP6nih5krP)
- 程海艳, 李明辉, 王宇. 党组织参与治理对国有上市公司盈余管理的影响[J]. 中国经济问题, 2020(02): 45-62.  [-CNKI-](https://kns.cnki.net/kcms/detail/detail.aspx?dbcode=CJFD&dbname=CJFDLAST2020&filename=ZJJW202002004&v=WYwf61CzD8Xnz92Tzx08QOACLl6J3cEUEHjKulzVqa36dbfKNuUtp%25mmd2FYjeryAA%25mmd2BT6)  
- ARGYLE BRONSON, NADAULD TAYLOR, PALMER CHRISTOPHER, et al. The Capitalization of Consumer Financing into Durable Goods Prices. The Journal of Finance, 2020, 76(1): 169-210.       [-PDF-](https://onlinelibrary.wiley.com/doi/pdf/10.1111/jofi.12977)

