在定理一中，我们说明了组内匹配估计量与加权个体固定效应估计量间的等价关系，那么 WFE 中的匹配权重 $w_{i t}^{i^{\prime} t^{\prime}}$ 和回归权重 $W_{i t}$ 该如何确定呢？

首先，我们从公式上进行分析：

$$
W_{i t}=D_{i t} \sum_{i^{\prime}=1}^{N} \sum_{t^{\prime}=1}^{T} w_{i t}^{i^{\prime} t^{\prime}}
$$

$$
w_{i t}^{i^{\prime} t^{\prime}}=\left\{\begin{array}{ll}
1 & \text { if }(i, t)=\left(i^{\prime}, t^{\prime}\right) \\
1 /\left|\mathcal{M}_{i^{\prime} t^{\prime}}\right| & \text { if }(i, t) \in \mathcal{M}_{i^{\prime} t^{\prime}} \\
0 & \text { otherwise. }
\end{array}\right.
$$

其中， $D_{i t}=1\left\{\left|\mathcal{M}_{i t}\right|>0\right\}$ 。

对于给定的观测 $(i,t)$ ，我们将它和所有的观测进行比较 ( $(i^{\prime}, t^{\prime})$ ， $i^{\prime}=1,2, \ldots, N$ 和 $t^{\prime}=1,2, \ldots, T$ ) 。如果 $(i,t)$ 恰好等于 $(i^{\prime}, t^{\prime})$ ，即当 $(i,t)$ 与自身比较时，那么匹配权重等于 1 ；如果 $(i,t)$ 属于 $(i^{\prime}, t^{\prime})$ 对应的匹配集合时，那么匹配权重等于 1 除以匹配集合中元素的个数；如果 $(i,t)$ 不符合上述两种情况的话，匹配权重为 0 。而回归权重就等于 $(i,t)$ 与所有观测进行比较的匹配权重之和。

我们举个例子进行更加具体的说明：

假设有一面板数据包含 100 家公司 ( $i = 1,2, \ldots, 100$ ) 和每家公司 10 年 ( $t = 1,2, \ldots, 10$ ) 的财务状况 (结果变量 $Y$ ) 和是否进行数字化转型升级 (处理变量 $X$ ) 。

$$
X_{i t}=\left\{\begin{array}{ll}
1 & \text { 公司进行了数字化转型升级 } \\
0 & \text { 公司未进行数字化转型升级 }
\end{array}\right.
$$

同时，简便起见，我们假设其符合 BA 设计 (无时间趋势) 且公司的数字化转型升级是一个不可逆的过程，即处理变量只会从 0 变为 1 且最多只变化一次。

我们采用 (12) 式的估计量来估计公司数字化转型升级对财务状况的平均当期影响：

$$\begin{aligned}
\hat{\tau}=\frac{1}{\sum_{i=1}^{N} C_{i}} & \sum_{i=1}^{N} C_{i}\left(\frac{\sum_{t=1}^{T} X_{i t} Y_{i t}}{\sum_{t=1}^{T} X_{i t}}\right.\left.-\frac{\sum_{t=1}^{T}\left(1-X_{i t}\right) Y_{i t}}{\sum_{t=1}^{T}\left(1-X_{i t}\right)}\right)
\end{aligned}  \quad (12)$$

其中， $C_{i}=1\left\{0<\sum_{t=1}^{T} X_{i t}<T\right\}$ 。(12) 式对应的匹配集合如下：

$$\mathcal{M}_{i t}=\left\{\left(i^{\prime}, t^{\prime}\right): i^{\prime}=i, X_{i^{\prime} t^{\prime}}=1-X_{i t}\right\}  \quad (13)$$

我们假设第 1 家公司在第 5 年进行了数字化转型升级，即 $X_{1 1}=X_{1 2}=X_{1 3}=X_{1 4}=0$ ， $X_{1 5}=X_{1 6}= \ldots =X_{1, 10}=1$ 。对于观测 $(i, t)=( 1 , 1 )$ ，当 $i^{\prime} \not = 1$ 时，显然 $(i, t)\not =(i^{\prime}, t^{\prime})$ ，并且 $(i, t)$ 不属于 $\mathcal{M}_{i^{\prime} t^{\prime}}$ ，因为 $(i, t)$ 和 $(i^{\prime}, t^{\prime})$ 不属于同一组。因此，匹配权重为 0 。当 $i^{\prime}=1$ 时，我们考虑 $t$ 的取值： $t^{\prime}=1$ 时， $(i, t)=(i^{\prime}, t^{\prime})$ ，因此匹配权重为 1 ；当 $t^{\prime}=2,3,4$ 时， $\mathcal{M}_{i^{\prime} t^{\prime}}$ 包含了组内所有处理组观测，即 $(1,5),(1,6),\ldots,(1,10)$ ，因此此时匹配权重为 0 ； 当 $t^{\prime}=5,6,\ldots,10$ 时， $\mathcal{M}_{i^{\prime} t^{\prime}}$ 包含了组内所有控制组观测，此时匹配权重为 $1 /\mathcal{M}_{i^{\prime} t^{\prime}}$ ，即 $\frac{1}{4}$ 。

接着，我们对 $(1,1)$ 的回归权重进行分析。由于第 1 家公司处理状况有变化，所以 $(1,1)$ 对应的匹配集合不为空集，即 $D_{i t} = 1$ 。回归权重等于匹配权重之和，因此在本例中， $(1,1)$ 的回归权重等于 $1+\frac{1} {4}\times 6 = \frac{5} {2}$ ，对于其他的观测，可以采用同样的方法计算其匹配权重与回归权重。同时，我们注意到由于 $W_{i t}=D_{i t} \sum_{i^{\prime}=1}^{N} \sum_{t^{\prime}=1}^{T} w_{i t}^{i^{\prime} t^{\prime}}$ ，因此如果某家公司的处理状况没有变化 (即其在观测窗口间没有进行数字化转型升级或在观测窗口前已进行过数字化转型升级) ，那么对于这家公司的所有观测， $D_{i t}=0$ , $t=1,2,\ldots,10$ ，进而 $W_{i t}=0$ 。我们可以认为在计算估计量时这家公司的所有观测先被自动删掉了，因此非参匹配估计量使用的观测数量远少于线性固定效应模型。

通过上述说明，我们了解了如何计算匹配权重和回归权重。实际上，匹配权重就是在估计反事实结果时使用的观测数量的倒数，即 $(i,t)$ 对于估计 $(i^{\prime}, t^{\prime})$ 的处理效应的贡献。我们考虑组内匹配估计量的一般形式：

$$\hat{\tau}=\frac{1}{\sum_{i=1}^{N} \sum_{t=1}^{T} D_{i t}} \sum_{i=1}^{N} \sum_{t=1}^{T} D_{i t}\left(\widehat{Y_{i t}(1)}-\widehat{Y_{i t}(0)}\right)  \quad (14)$$

其中，当 $X_{i t}=x$ 时， $Y_{i t}(x)$ 可以被观测；当 $X_{i t}=1-x$ 时，我们使用 $(i, t)$ 匹配集合的平均结果来估计 $Y_{i t}(x)$ ：

$$\widehat{Y_{i t}(x)}=\left\{\begin{array}{ll}
Y_{i t} & \text { if } X_{i t}=x \\
\frac{1}{\left|\mathcal{M}_{i t}\right|} \sum_{\left(i^{\prime}, t^{\prime}\right) \in \mathcal{M}_{i t}} Y_{i^{\prime} t^{\prime}} & \text { if } X_{i t}=1-x
\end{array}\right.  \quad (15)$$

还是使用上述举例的面板数据：对于第 1 家公司第 5-10 年的数据，我们只能观测到数字化转型升级后公司的财务状况，我们需要通过其对应的匹配集合中观测结果的平均数来估计反事实结果。更为具体的，对于观测 $(1,5)$ ， $X_{1 5}=1$ ， $\widehat{Y_{1 5}(1)} = Y_{1 5}$ ， $\widehat{Y_{1 5}(0)} = \frac{1} {4}(Y_{1 1}+Y_{1 2}+Y_{1 3}+Y_{1 4})$ ，而 $w_{1 1}^{1 5}$ 恰好等于 $\frac{1} {4}$ 。
