
&emsp;

> **作者**：吕可夫 (厦门大学)    
> **邮箱**：<lvkefu@163.com>  

&emsp;

&emsp;

---

**目录**
[[toc]]

---

&emsp;

&emsp;

## 1. 问题背景

在许多研究中，非时变变量在结构方程中扮演着重要的角色。例如在劳动经济学、卫生经济学等领域中，学者们热衷于讨论非时变变量 (如教育、性别、国籍、种族和宗教背景) 可能导致的经济后果。

需要注意的是，这些研究在开展过程中或多或少会面临一些估计问题：

- 在常见的固定效应静态面板模型中，非时变的变量由于与个体固定效应完全共线性，无法估计出系数；
- 在动态面板模型中，使用 GMM 估计方法的前提是需要满足正交性假设，否则非时变变量的估计系数也是不一致且有偏的 (Arellano 和 Bond，1991；Arellano 和 Bover，1995；Blundell 和 Bond，1998)。

值得一提的是，一种新的两阶段估计程序 `xtseqreg`，可以在不依赖正交性假设的情况下估计非时变变量的系数 (Kripfganz1 和 Schwarz，2019)。

&emsp;

## 2. 估计原理

`xtseqreg` 命令的主要目的是估计带有非时变变量的线性 (动态) 面板数据模型。具体估计流程可分为以下两个步骤：

- 第一阶段只对时变变量回归估计其系数，并提取回归的残差；
- 第二阶段将该残差对非时变变量进行回归，获取非时变变量的系数。

以如下动态面板模型为例：

$$
y_{i t}=\lambda y_{i, t-1}+\mathbf{x}_{i t}^{\prime} \boldsymbol{\beta}+\mathbf{f}_{i}^{\prime} \boldsymbol{\gamma}+e_{i t}, \quad e_{i t}=\alpha_{i}+u_{i t}
$$

其中个体 $i$ 取值为 $1,2,3,...,N$，时间 $t$ 取值为 $1,2,3,...,T$，$\mathbf{x}_{i t}$ 是 $K_{x}×1$ 维向量，代表随时间变化的自变量，$\mathbf{f}_{i}$ 是 $K_{f}×1$ 维向量，代表非时变自变量。$\alpha_{i}$ 代表不可观测的个体固定效应。

实现该两阶段估计方法需要满足两条假设：

- 假设 1：扰动项 $u_{i t}$ 和个体效应 $\alpha_{i}$ 在个体 $i$ 之间独立分布，且对任意 $s \neq t$ 满足 $E\left[u_{i t}\right]=E\left[\alpha_{i}\right]=0$，$E\left[u_{i s} u_{i t}\right]=0$ ，以及 $E\left[\alpha_{i} u_{i t}\right]=0$；
- 假设 2：相对于扰动项 $u_{i t}$，$\mathbf{x}_{i t}$ 和  $\mathbf{f}_{i}$ 严格外生，即 $E\left[u_{i t} \mid \mathbf{x}_{i 0}, \mathbf{x}_{i 1}, \ldots, \mathbf{x}_{i T}, \mathbf{f}_{i} ; \alpha_{i}\right]=0$。

在第一阶段里，我们先把非时变变量与个体固定效应合并为 $\eta_{i}=\mathbf{f}_{i}^{\prime} \gamma+\alpha_{i}$，则第一阶段模型可改写为如下方程：

$$
y_{i t}=\lambda y_{i, t-1}+\mathbf{x}_{i t}^{\prime} \boldsymbol{\beta}+\bar{\eta}+\epsilon_{i t}, \; \epsilon_{i t}=\eta_{i}-\bar{\eta}+u_{i t}
$$

在满足假设 1 和假设 2 的情况下，一阶段方程可使用多种常规估计方法，例如 Hsiao 等 (2002) 的 QML 估计量，Arellano 和 Bond (1991)、以及 Blundell 和 Bond (1998) 基于线性矩条件的 GMM 估计量，Ahn 和 Schmidt (1995) 基于非线性矩条件的 GMM 估计量等。

接下来将一阶段估计的残差提取出来，采用如下水平方程回归即可得到非时变变量的估计系数：

$$
y_{i t}-\hat{\lambda} y_{i, t-1}-\mathbf{x}_{i t}^{\prime} \hat{\boldsymbol{\beta}}=\mathbf{f}_{i}^{\prime} \gamma+v_{i t}
$$

$$
\quad v_{i t}=\alpha_{i}+u_{i t}-(\hat{\lambda}-\lambda) y_{i, t-1}-\mathbf{x}_{i t}^{\prime}(\hat{\boldsymbol{\beta}}-\boldsymbol{\beta})
$$

此外，上述两个阶段均可以使用工具变量解决自变量的内生性问题。在第二阶段，本方法纠正了通常的标准误，标准误修正也是这个新命令的主要贡献之一。

另外，`xtseqreg` 命令也可以用于估计常规 IV/GMM 单阶段。但本命令并非要替代以往命令，在某种程度上，其他命令包含一些本命令所不能实现的功能，但是本命令相比之下提供了额外的灵活性，且实现了标准误的校正。

&emsp;

## 3. xtseqreg 命令

```stata
*命令安装
cnssc install xtseqreg, replace 
```

```stata
*命令语法
xtseqreg depvar [(indepvars1)] [indepvars2] [if] [in] [, options]
```

`options` 介绍：

- `first(first_spec)`：指定第一阶段估算结果；
- `both`：指定第一阶段估算结果；
- `iv(iv_spec)`：普通工具变量，可指定多个；
- `gmmiv(gmmiv_spec)`：GMM 估计工具变量，可指定多个；
- `wmatrix(wmat_spec)`：指定初始权重矩阵；
- `twostep`：两步法估计；
- `teffects`：纳入时间效应；
- `noconstant`：无截距项；
- `vce(vcetype)`：`vcetype` 可以是 `conventional`、`ec`、`robust` 或 `cluster-clustvar`；
- `combine`：合并两个方程的估计结果；
- `level(#)`：设定置信水平，默认 95%；
- `noheader`：不显示标题；
- `notable`：不显示系数表；
- `noomitted`：不显示省略的变量。

&emsp;

## 4. Stata 范例

本文案例数据为一个劳动-收入领域的面板数据。我们关心的被解释变量是 **lwage**，即对数工资，**id** 和 **t** 则分别对应面板的横截面和时间指标。此外数据中包含了大量不随时间变化的变量，例如是否为黑人 **blk**、性别 **fem** 等等。固定效应面板回归通常无法估计这些非时变变量。

```stata
. webuse psidextract, clear
. des

Contains data from https://www.stata-press.com/data/r17/psidextract.dta
 Observations:         4,165                  
    Variables:            22                  18 Aug 2020 11:53
                                              (_dta has notes)
--------------------------------------------------------------------------
Variable      Storage   Display    Value
    name         type    format    label      Variable label
--------------------------------------------------------------------------
exp             byte    %9.0g                 Years of full-time work experience
wks             byte    %9.0g                 Weeks worked
occ             byte    %15.0g     occ        Occupation
ind             byte    %17.0g     ind        Industry
south           byte    %9.0g      south      Residence
smsa            byte    %9.0g      smsa       SMSA
ms              byte    %11.0g     ms         Marital status
fem             byte    %9.0g      fem        Female
union           byte    %18.0g     union      Wage set by union contract
ed              byte    %9.0g                 Years of education
blk             byte    %9.0g      blk        Race
lwage           float   %9.0g                 Log wage
id              int     %9.0g                 ID
t               byte    %9.0g                 Time
tdum1           byte    %8.0g                 t== 1.0000
tdum2           byte    %8.0g                 t== 2.0000
tdum3           byte    %8.0g                 t== 3.0000
tdum4           byte    %8.0g                 t== 4.0000
tdum5           byte    %8.0g                 t== 5.0000
tdum6           byte    %8.0g                 t== 6.0000
tdum7           byte    %8.0g                 t== 7.0000
exp2            int     %9.0g                 
-------------------------------------------------------------------------
```

我们不妨设待估模型为包含被解释变量 `lwage` 一阶和二阶滞后项的动态面板模型，控制变量中时变变量包含 **exp**、**exp2**、**occ**、**ind**、**union** (**occ**、**ind**、**union** 虽然是虚拟变量，但并非随时间完全不变化，故而在一阶段中作为时变变量进行估计)，而我们关心的非时变变量为 **ed**、**fem**、**blk**。

$$
lwage_{i t}=\hat{\lambda_{1}} lwage_{i, t-1}+\hat{\lambda_{2}} lwage_{i, t-2}+\mathbf{x}_{i t}^{\prime} \hat{\boldsymbol{\beta}}+\mathbf{f}_{i}^{\prime} \gamma+v_{i t}
$$

首先进行第一阶段回归，将非时变变量全省略，仅把被解释变量 **lwage** 对所有时变变量回归。采用的估计方法为 Arellano/Bond 的两阶段差分 GMM (two-step difference-GMM estimator)，外生变量的 IV 则为其本身，标准差则使用 Windmeijer 修正的稳健标准误。

```stata
. xtseqreg L(0/2).lwage exp exp2 occ ind union, gmmiv(L.lwage, model(difference) ///
>          lagrange(1 4) collapse) iv(exp exp2 occ ind union, difference         ///
>          model(difference)) twostep vce(robust)

Group variable: id                           Number of obs         =      2975
Time variable: t                             Number of groups      =       595
                                             Obs per group:    min =         5
                                                               avg =         5
                                                               max =         5
                                             Number of instruments =        10
                                   (Std. err. adjusted for 595 clusters in id)
------------------------------------------------------------------------------
             |              WC-Robust
       lwage | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
       lwage |
         L1. |      0.366      0.172     2.12   0.034        0.028       0.703
         L2. |      0.101      0.073     1.38   0.168       -0.043       0.244
         exp |      0.050      0.028     1.78   0.076       -0.005       0.105
        exp2 |     -0.000      0.000    -1.39   0.164       -0.000       0.000
         occ |     -0.043      0.028    -1.51   0.131       -0.098       0.013
         ind |      0.048      0.031     1.58   0.115       -0.012       0.108
       union |      0.007      0.029     0.24   0.808       -0.049       0.063
       _cons |      2.738      1.088     2.52   0.012        0.605       4.870
------------------------------------------------------------------------------
```

在上述第一阶段估计完成后，残差将自动存储，作为被解释变量用于第二阶段的估计。作为一个例子，不妨假设 **ed** 在二阶段估计中为内生变量，我们使用 **occ** 作为其 IV。

```stata
. xtseqreg lwage (L(1/2).lwage exp exp2 occ ind union) ed fem blk, ///
>          iv(occ fem blk, model(level)) vce(robust)

Group variable: id                           Number of obs         =      2975
Time variable: t                             Number of groups      =       595
------------------------------------------------------------------------------
Equation _first                              Equation _second
Number of obs         =      2975            Number of obs         =      2975
Number of groups      =       595            Number of groups      =       595
Obs per group:    min =         5            Obs per group:    min =         5
                  avg =         5                              avg =         5
                  max =         5                              max =         5
Number of instruments =        10            Number of instruments =         4
                                     (Std. err. adjusted for clustering on id)
------------------------------------------------------------------------------
             |               Robust
       lwage | Coefficient  std. err.      z    P>|z|     [95% conf. interval]
-------------+----------------------------------------------------------------
_first       |
       lwage |
         L1. |      0.366      0.172     2.12   0.034        0.028       0.703
         L2. |      0.101      0.073     1.38   0.168       -0.043       0.244
         exp |      0.050      0.028     1.78   0.076       -0.005       0.105
        exp2 |     -0.000      0.000    -1.39   0.164       -0.000       0.000
         occ |     -0.043      0.028    -1.51   0.131       -0.098       0.013
         ind |      0.048      0.031     1.58   0.115       -0.012       0.108
       union |      0.007      0.029     0.24   0.808       -0.049       0.063
       _cons |      2.738      1.088     2.52   0.012        0.605       4.870
-------------+----------------------------------------------------------------
_second      |
          ed |      0.063      0.035     1.82   0.068       -0.005       0.132
         fem |     -0.097      0.058    -1.68   0.093       -0.209       0.016
         blk |     -0.153      0.101    -1.52   0.129       -0.351       0.045
       _cons |     -0.794      0.442    -1.80   0.072       -1.660       0.072
------------------------------------------------------------------------------
```

以上两阶段的估计流程主要是用于演示，`xtseqreg` 命令也可以一次性估计上述两个阶段，具体命令为：

```stata
. xtseqreg lwage (L(1/2).lwage exp exp2 occ ind union) ed fem blk,               ///
>          gmmiv(L.lwage, model(difference) lagrange(1 4) collapse equation(#1)) ///
>          iv(exp exp2 occ ind union, difference model(difference) equation(#1)) ///
>          iv(occ fem blk, model(level) equation(#2)) twostep vce(robust) both
```

在模型估计结束后，可以使用 `estat overid` 命令对两个阶段同时进行 Hansen's J-test，以检验是否存在过度识别问题。

```stata
. estat overid

Hansen's J-test for equation _first                    chi2(2)     =    0.2935
H0: overidentifying restrictions are valid             Prob > chi2 =    0.8635

Hansen's J-test for equation _second                   chi2(0)     =    0.0000
note: coefficients are exactly identified              Prob > chi2 =         .
```

&emsp;

## 5. 参考资料

- XTSEQREG: new Stata command for sequential / two-stage (GMM) estimation of linear panel models [-Link-](https://www.statalist.org/forums/forum/general-stata-discussion/general/1374005-xtseqreg-new-stata-command-for-sequential-two-stage-gmm-estimation-of-linear-panel-models)
- Kripfganz S, Schwarz C. Estimation of linear dynamic panel data models with time‐invariant regressors[J]. Journal of Applied Econometrics, 2019, 34(4): 526-546. [-PDF-](https://www.econstor.eu/bitstream/10419/78229/1/756554209.pdf)

&emsp;

## 6. 相关推文

> Note：产生如下推文列表的 Stata 命令为：   
> &emsp; `lianxh 两阶段 动态面板 工具变量, m`  
> 安装最新版 `lianxh` 命令：    
> &emsp; `ssc install lianxh, replace` 

- 专题：[专题课程](https://www.lianxh.cn/blogs/44.html)
  - [⏩直播：动态面板数据模型](https://www.lianxh.cn/news/594aa12c096ca.html)
  - [FAQs答疑-2021寒假-Stata高级班-Day1-连玉君-动态面板](https://www.lianxh.cn/news/14e96b04d292b.html)
- 专题：[结果输出](https://www.lianxh.cn/blogs/22.html)
  - [Stata结果输出：两阶段回归的结果输出](https://www.lianxh.cn/news/eb93877bb3ba0.html)
- 专题：[面板数据](https://www.lianxh.cn/blogs/20.html)
  - [Stata：动态面板数据模型OLS估计的偏差](https://www.lianxh.cn/news/dba7c3067ae0a.html)
  - [Stata实操陷阱：动态面板数据模型](https://www.lianxh.cn/news/cc6c5ea80d70c.html)
  - [Stata: 动态面板门槛模型](https://www.lianxh.cn/news/f77a4e5c6206f.html)
- 专题：[IV-GMM](https://www.lianxh.cn/blogs/38.html)
  - [IV在哪里？奇思妙想的工具变量](https://www.lianxh.cn/news/946348365bb2d.html)
  - [twostepweakiv：弱工具变量有多弱？](https://www.lianxh.cn/news/2e36ddefa9f76.html)
  - [多个(弱)工具变量如何应对-IV-mivreg？](https://www.lianxh.cn/news/807b616b11aae.html)
  - [IV：工具变量不满足外生性怎么办？](https://www.lianxh.cn/news/c66c145337ffa.html)
  - [IV-工具变量法：第一阶段系数符号确定时的小样本无偏估计](https://www.lianxh.cn/news/00ada06e01553.html)
  - [IV：可以用内生变量的滞后项做工具变量吗？](https://www.lianxh.cn/news/c45f6113e2c2a.html)
  - [Stata: 工具变量法 (IV) 也不难呀！](https://www.lianxh.cn/news/b4c8cc6e6d1ba.html)
  - [IV-估计：工具变量不外生时也可以用！](https://www.lianxh.cn/news/9ec69d0779fb4.html)



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;
