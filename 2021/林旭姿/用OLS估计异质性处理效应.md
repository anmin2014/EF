&emsp;

> **作者**：林旭姿 ( 中山大学 )  
> **E-mail：**<lamyc@mail2.sysu.edu.cn>

> **Source：** Słoczyński, Tymon. 2020. “Interpreting OLS Estimands When Treatment Effects Are Heterogeneous: Smaller Groups Get Larger Weights.” 2021 Stata Conference 11, Stata Users Group. The Review of Economics and Statistics, 1–27. [-PDF-](https://arxiv.org/pdf/1810.01576.pdf)

&emsp;

---

**目录**
[toc]

---

&emsp;

## 1 引言

本推文基于 Tymon Sloczynski 于 2019 年发表的文献，对其进行了核心思想与研究方法的提炼与分析。作者 Sloczynski 深入地解释了当处理效应异质时，OLS 估计量 $\tau$ 的含义，并得出了如下结论：
&emsp;

1、OLS 估计量 $\tau$ 实质上是两个参数的凸组合 ( convex combination ) 。在特定条件下，这两个参数可以被解释为干预组的平均处理效应 ( ATT ) 与对照组的平均处理效应 ( ATU ) 。
&emsp;

2、$\tau$ 是这两个参数的加权平均，其各自权重分别与每个组别的观测值在总体里的占比反向相关。即，当越多的个体被干预，ATT 系数的权重就越小。
&emsp;

## 2 研究背景介绍

现有的研究主要是在保持控制变量 x 的向量不变的情况下，使用线性模型研究二元变量 ( 干预 ) 在结果的期望值上的效应。尽管存在着大量半参数或者非参数影响着平均处理效应，大量的研究人员还是坚持使用传统回归方法来进行分析。特别的，许多文献使用了最小二乘法 ( OLS ) 来估计 $$y=\alpha +\tau d+ X\beta+ u \tag{1}， \label{eq:test}$$
其中，y 代表结果，d 代表干预，X 则为决策变量的行向量，即 ( $x_{1}$, ... ,$x_{k}$ ) 。通常来说，$\tau$ 被解释为平均处理效应 ( ATE ) 。
&emsp;

然而，已有大量文献强调了处理效应异质时对于 OLS 估计的影响。由于在处理效应异质情况下，由 OLS 估计出的两个组别中的平均效应系数的权重是与每个组别中观测值在总体里的占比反向相关的。这时候，当越多的个体被干预时，相比于其观测值在总体中的占比，ATT 系数的权重就越小。
&emsp;

若干预组和对照组观测值的占比存在差异，两个组别中权重赋予的差异 ( 即各自赋予的权重并不等于两组观测值在总体中的占比 ) 则会使得 OLS 估计值 $\tau$ 偏离 ATE 的估计。因此当处理效应异质时仍然坚持使用形如模型 ( 1 ) 的 OLS 估计是不合适的。换言之，只有在干预组和对照组的观测值占比相近时，才能利用 OLS 法给出 ATE 的近似估计。

## 3 理论部分

### 3.1 引例：NSW 项目对于收入的影响

NSW 项目 ( National Supported Work Demonstration ) ，即美国国家支持工作示范项目，是一个暂时性的工作提供项目，旨在帮助缺乏基本工作技能的残疾人士进入劳动力市场，并且能够为残疾人士提供工作机会与庇护场所。与其他支持性项目不同的是，NSW 项目随机抽取申请者为其提供工作机会与培训，因此被选中的残疾人士( 干预组 )便能享受到 NSW 项目带来的全部福利，而那些未被选中的申请者 ( 对照组 )只能自行谋生。&emsp;

然而，在该项目中，其处理效应却存在着异质性：对于参与了 NSW 项目的劳工 ( 干预组 ) 来说，分配给他们的工作往往是不同的。例如，有的人被分配到了加油站工作，有的人被分配到了打印店工作。特别的，男性与女性被分配到的工作往往十分不同：女性往往被分配到服务业，而男性总是被分配到制造业。在由于分配导致的处理效应异质的情况下，本推文将在之后的小节利用 NSW-CPS 的数据，分析该项目对于劳工收入的影响。&emsp;

### 3.2 OLS 加权平均数的说明

如果用 L ( $\cdot$ | $\cdot$ ) 来表示线性估计，为了估计 $d$ 和 $X$，我们可以得到：$$L(y|1,d,X)=\alpha+\tau d+X\beta \tag{2}，$$
并且，我们的关注点在于该式中的 $\tau$。
我们令 $$\rho=P ( d=1 ) ， \tag{3}$$ 以此来表示“控制”的非条件概率。
然后再定义一个倾向得分
$$p(X)=L ( d|1,X ) =\alpha_{p}+X \beta_{p} \tag{4}，$$
该线性模型是真实倾向得分的最好的线性估计。
然而，由于模型 ( 2 ) 中可能包含幂次项或者交叉项，我们可以把模型 ( 2 ) 看作是部分线性的。
&emsp;

在定义了 $p(X)$ 后，考虑到干预组 ( $d=1$ ) 和对照组 ( $d=0$ ) 的的差别，分别定义 $y$ 对于 $p(X)$ 的线性估计：
$$L[y|1,p(X),d=1]=\alpha_{1}+\gamma_{1}\cdot p(X) \tag{5}$$
与
$$L[y|1,p(X),d=0]=\alpha_{0}+\gamma_{0}\cdot p(X)。 \tag{6}$$
值得注意的是，模型 ( 4 ) ( 5 ) ( 6 ) 是被定义出来的，它们能够保证最终推导结果的存在性与唯一性。
&emsp;

为了进行进一步的推导，我们还需要进行如下假设：
&emsp;

**假设 1.**&emsp;

E($y^{2}$)与 E($||X||^{2}$)是有限的。
&emsp;

$(d,X)$ 的协方差矩阵是非奇异矩阵。
&emsp;

**假设 2.**&emsp;V[$p(X)$|$d=1$]与 V[$p(X)$|$d=0$]非零。其中，V[ $\cdot$ | $\cdot$ ]为条件方差，与 E[$p(X)$|$d=j$],($j=0,1$)有关。

假设 1 确保了模型 ( 2 ) 与 ( 4 ) 中线性估计的存在性与唯一性。相似的，假设 2 确保了模型 ( 5 ) 与 ( 6 ) 中线性估计的存在性与唯一性。&emsp;

在以上假设的基础上，我们可以将 $d$ 的平均部分线性效应定义为
$$\tau_{APLE}=(\alpha_{1}-\alpha_{0})+(\gamma_{1}-\gamma_{0})\cdot E[p(X)] \tag{7}$$
对于第 j 组(j=0,1)的 d，我们同样可以定义其平均部分线性效应：
$$\tau_{APLE,j}=(\alpha_{1}-\alpha_{0})+(\gamma_{1}-\gamma_{0})\cdot E[p(X)|d=j] \tag{8}$$
基于假设 1 和假设 2 而得出的以上估计量是具有“因果解释的”，我们将在下一节进行详细的论证。&emsp;

在假设 1 与假设 2 的基础上，基于一系列的数学推导 ( 本推文省略了证明过程，有兴趣的读者可以下载作者原文深入学习 ) ，我们可以得出以下具有普遍性的结论：&emsp;

**结论 1.** ( OLS 加权平均数的说明 )
&emsp;

基于假设 1 与假设 2，
$$\tau=\omega_{1}\cdot \tau_{APLE,1}+\omega_{0}\cdot \tau_{APLE,0}$$
其中，&emsp;

$$\omega_{1}=\frac{(1-\rho)\cdot V[p(X)|d=0]}{\rho \cdot V[p(X)|d=1]+(1-\rho) \cdot V[p(X)|d=0]}，$$
$$\omega_{0}=1-\omega_{1}=\frac{\rho\cdot V[p(X)|d=1]}{\rho \cdot V[p(X)|d=1]+(1-\rho) \cdot V[p(X)|d=0]}，$$

从结论 1 可以看出 OLS 估计量 $\tau$ 是 $\tau_{APLE,1}$ 和 $\tau_{APLE,0}$ 的凸组合。通过对于 $\tau_{APLE,j}$ 的定义，我们可以很容易的解出 $\tau$：&emsp;

第一步：获得 $p(X)$，即倾向得分。&emsp;

第二步：对于干预组 ( $d=1$ ) 与对照组 ( $d=0$ ) ，通过模型 ( 8 ) ，分别获得 $y$ 对于 $p(X)$ 的线性估计，由此得到 $\tau_{APLE,1}$ 与 $\tau_{APLE,0}$。&emsp;

第三步：计算 $\tau_{APLE,1}$ 与 $\tau_{APLE,0}$ 的加权平均，以此获得 $\tau$。&emsp;

其中，$\tau_{APLE,1}$ 的权重，即 $\omega_{1}$，随着 $\frac{V[p(X)|d=1]}{V[p(X)|d=0]}$ 与 $\rho$ 的增大而减小；$\tau_{APLE,0}$ 的权重，即 $\omega_{0}$，随着 $\frac{V[p(X)|d=1]}{V[p(X)|d=0]}$ 与 $\rho$ 的增大而增大。本推文省略了其证明过程，感兴趣的读者可以下载原文自行学习。&emsp;

$\omega$ 与 $\rho$ 间的权重关系揭示了这样一个结论：当第 $j$ 组所包含的个体越多，赋予 $\tau_{APLE,j}$ ( 即该组的效应 ) 的权重就越少。很显然的，这个结果将导致 OLS 估计出的 APLE 存在偏差，因为 $\tau_{APLE}=\rho \cdot \tau_{APLE,1}+(1-\rho)\cdot \tau_{APLE,0}$。

### 3.3 因果关系的说明

尽管结论 1 在假设 1 与假设 2 的条件下具有普遍性，我们仍然不能保证 $\tau_{APLE,1}$ 与 $\tau_{APLE,0}$ 能够解释因果关系。为解释因果关系，我们首先需要定义两个潜在结果，即 $y(1)$ 与 $y(0)$，在任意个体中只有一个结果能够被观测到。而 ATE,ATT 与 ATU 则分别被定义为 $\tau_{ATE}=E[y(1)-y(0)]$，$\tau_{ATT}=E[y(1)-y(0)|d=1]$，$\tau_{ATY}=E[y(1)-y(0)|d=0]$。为了解释因果关系，我们还需要如下假设：&emsp;

**假设 3.**&emsp;

$$E[y(1)|X,d]=E[y(1)|X]$$
$$E[y(0)|X,d]=E[y(0)|X] $$
**假设 4.**&emsp;

$$E[y(1)|X]=\alpha_{1}+\gamma_{1}\cdot p(X)$$
$$E[y(0)|X]=\alpha_{0}+\gamma_{0}\cdot p(X)$$

假设 3 和假设 4 保证 $\tau$ 能够解释因果关系。其中，假设 4 的充分非必要条件是，$d$ 的条件均值关于 $X$ 应当是线性的，且 $y(1)$ 与 $y(0)$ 关于真实倾向得分 $p(X)$ 也应当是线性的。&emsp;

因此，基于以上两个假设与结论 1，我们可以得到如下推论。&emsp;

**推论 1.** ( OLS 因果关系的说明 )&emsp;

基于假设 1,2,3 和 4，有
$$\tau=\omega_{1}\cdot \tau_{ATT,1}+\omega_{0}\cdot \tau_{ATU,0}$$

这个推论是易于证明的。由于假设 3 表明 E$[y(1)-y(0)|X]$=E$[y|X,d=1]$$-$E$[y|X,d=0]$，假设 4 表明 E$[y(1)-y(0)|X]$=$(\alpha_{1}-\alpha_{0})+(\gamma_{1}-\gamma_{0})\cdot$ E$[$p(X)$]$，因此我们可以得出 $\tau_{ATT}=\tau_{APLE,1}$，$\tau_{ATU}=\tau_{APLE,0}$。&emsp;

推论 1 揭示了，在假设 1,2,3 和 4 的前提下，从结论 1 中得到的 OLS 权重 $\omega_{1},\omega_{0}$ 是能够适用于 $\tau_{ATT}$ 与 $\tau_{ATU}$ 的。因此，$\tau$ 是能够解释因果关系的。当被干预的个体在总体里的占比越多，$\tau_{ATT}$ 的 OLS 权重就越小。再次强调，这个结果将导致 OLS 估计出的 ATE 存在偏差，因为 $\tau_{ATE}=\rho \cdot \tau_{ATT}+(1-\rho)\cdot \tau_{ATU}$。&emsp;

回想我们最初使用模型 ( 1 ) 以及 OLS 的动机，是因为 $y$ 对于 $d$ 与 $X$ 的线性估计是对于给定 $d$ 和 $X$ 的情况下对于 $y$ 的最好的线性估计。然而，如果我们的目标是进行因果推断，那么我们就不应当出于这个动机而使用 OLS 估计，这并不是一个好办法。最小二乘法对于预测实际结果是十分“好”的，尽管如此，因果推断则是为了预测缺失的结果 ( missing outcomes )，其被定义为 $y_{m}=y(1)\cdot (1-d)+y(0)\cdot d$。换句话说，OLS 权重对于预测“是什么”是最优的，而我们的关注点在于，当干预的分配存在差异时，将会“发生什么”。&emsp;

直觉告诉我们，如果我们想在保证普遍性的情况下预测“是什么”，第 1 组 $(  d=1 )$ 包括的个体数量就应当远大于第 0 组 $( d=0 )$，我们更乐意将较大的权重赋予给第 1 组的线性估计系数 ( $\alpha_{1}和\gamma_{1}$ ) ，这是因为这些系数能够被用于预测该组的实际结果。实际上，结论 1 同样表明了
$$\tau=[E(y|d=1)-E(y|d=0)]-(\omega_{0}\gamma_{1}+\omega_{1}\gamma_{0})\cdot(E[p(X)|d=1]-E[p(X)|d=0]) \tag{9}$$
从上式可以得出，当第 1 组变“大”时，$\gamma_{1}$ 的权重 $\omega_{0}$ 也会相应变大。&emsp;

然而，如果第 1 组变“大”了，而我们的目标是想要预测“未能观测到的结果”，我们就需要赋予 $\alpha_{0}$ 以及 $\gamma_{0}$ 更大的权重，这是因为这两个系数能够用于预测第 1 组的反事实的结果。当 $y(1)$ 和 $y(0)$ 的条件均值关于 $X$ 是线性的时候，我们可以写出如下式子：
$$\tau_{ATE}=[E(y|d=1)-E(y|d=0)]-[(1-\rho)\beta_{1}+\rho\beta_{0}]\cdot[E(X|d=1)-E(X|d=0)] \tag{10} $$
其中，$\beta_{1}$ 和 $\beta_{0}$ 是分别在 $y(1)$ 和 $y(0)$ 情况下估计出来的 $X$ 的系数。等式(9)(10)重申了推论 1 的观点，即 $\tau$ 与 $\tau_{ATE}$ 有着十分相似的结构，但他们存在实质上的不同：即参数权重的分配。实际上，当第 1 组变“大”时，$\beta_{1}$ 的权重 $1-\rho$ 会变小。这与我们从 OLS 看到的结果是相反的。

### 3.4 OLS 加权平均数的的推广

在这一小节，我们将会把将 $\tau$ 与 $\tau_{ATE}$ 以及 $\tau$ 与 $\tau_{ATT}$ 间的差异分解为以下几个元素的估计：&emsp;

1、$\tau_{APLE,1}$ 与 $\tau_{ATT}$ 间的差异。&emsp;

2、$\tau_{APLE,0}$ 与 $\tau_{ATU}$ 间的差异。 ( 非线性导致的偏误 ) &emsp;

3、$\tau_{ATT}$ 与 $\tau_{ATU}$ 的 OLS 权重。 ( 异质性导致的偏误 ) &emsp;

在假设 3 与假设 4 的基础上，我们将在如下内容探讨异质性导致的偏误的来源。&emsp;

**推论 2.** &emsp;在假设 1 与假设 2 的基础上，有
$$\tau-\tau_{ATE}=\underset{非线性导致的偏误}{\underbrace{\omega_{0}\cdot(\tau_{APLE,0}-\tau_{ATU})+\omega_{1}\cdot(\tau_{APLE,1}-\tau_{ATT})}}+\underset{异质性导致的偏误}{\underbrace{\delta\cdot(\tau_{ATU}-\tau_{ATT})}}$$
其中，$\delta=\rho-\omega_{1}=\frac{\rho^{2}\cdot V[p(X)|d=1]-(1-\rho)^{2}\cdot V[p(X)|d=0]}{\rho\cdot V[p(X)|d=1]+(1-\rho)\cdot V[p(X)|d=0]}$。同样，在假设 1,2,3 和 4 下，我们可以得到
$$\tau-\tau_{ATE}=\delta\cdot(\tau_{ATU}-\tau_{ATT})$$
**推论 3.** &emsp;在假设 1 与假设 2 的基础上，有
$$\tau-\tau_{ATT}=\underset{非线性导致的偏误}{\underbrace{\omega_{0}\cdot(\tau_{APLE,0}-\tau_{ATU})+\omega_{1}\cdot(\tau_{APLE,1}-\tau_{ATT})}}+\underset{异质性导致的偏误}{\underbrace{\omega_{0}\cdot(\tau_{ATU}-\tau_{ATT})}}$$
同样，在假设 1,2,3 和 4 下，我们可以得到
$$\tau-\tau_{ATT}=\omega_{0}\cdot(\tau_{ATU}-\tau_{ATT})$$

利用简单的代数学即可完成推论 2 与推论 3 的证明，感兴趣的读者可以自行推导。&emsp;

以上两个推论表明，不管我们关注的是 $\tau_{ATE}$ 还是 $\tau_{ATT}$，异质性导致的偏误就相当于 $\tau_{ATU}$ 与 $\tau_{ATT}$ 间差异（即 $\tau_{ATU}-\tau_{ATT}$）的衡量。不过，在假设 1 与假设 2 的基础上，$\omega_{0}$ 总为正，而 $\delta$ 却有可能为负。尽管如此，$\omega_{0}$ 与 $\delta$ 的绝对值却总是介于 0 与 1 之间。因此，$\omega_{0}$ 与 $|\delta|$ 能够被用来解释异质性偏离 ( 即 $\tau_{ATU}-\tau_{ATT}$ ) 的“程度”。这时候，在使用 OLS 估计模型 ( 1 ) 时，报告出 $\omega_{0}$ 与 $\delta$ 这两个参数对于分析便十分有帮助。&emsp;

**假设 5.** &emsp;$V[p(X)|d=1]=V[p(X)|d=0]$&emsp;
有了假设 5，$\delta$ 与 $\omega_{0}$ 的计算便能大大被简化。若用 $\delta^{*}$ 与 $\omega_0^*$ 来表示 $\delta$ 与 $\omega_{0}$ 的值，我们可以写出 $\delta^{*}=2\rho-1$，$\omega_0^*=\rho$。因此，若想获得 $\delta$ 与 $\omega_{0}$，我们只需要 $\rho$，即 $d=1$ 的个体在总体的占比。 &emsp;

在假设 5 的基础上，结合推论 2 与推论 3，当 $\rho$ 等于 0%，50% 或者 100% 时，我们可以很清楚的看到结果。当极少的个体被干预时，$\tau\cong\tau_{ATT}$；当大部分的个体都被干预时，$\tau\cong\tau_{ATU}$；最后，当干预组与对照组的个体数量相近时，$\tau\cong\tau_{ATE}$。这个结果同样会体现在推论 4 中。&emsp;

**推论 4** &emsp;在假设 1,2 和 5 的基础上
$$\tau=(1-\rho)\cdot\tau_{APLE,1}+\rho\cdot\tau_{APLE,0}$$
同样，在假设 1,2,3,4 和 5 的基础上，
$$\tau=(1-\rho)\cdot+\rho\cdot\tau_{APLE,0}$$
利用简单的代数学即可完成推论 4 的证明，感兴趣的读者可以自行推导。&emsp;

推论 4 说明了在特定条件下，OLS 颠倒了 $\tau_{APLE,1}$ 与 $\tau_{APLE,0}$“原本的权重”。在假设 5 的基础上，$\tau$ 是特定群体的效应的凸组合，且赋予各自效应的权重是与其观测值占比的大小相反的。即 $d=1$ 的个体的占比被赋予给第 0 组的 $d$ 的平均效应作为权重，反之亦然。&emsp;

本小节获得的结论将有助于研究人员在处理效应异质时分析 OLS 估计量的含义。接下来本推文将引入实例进行 Stata 实操，借助实例以便更为形象地对 OLS 估计量进行分析。

## 4 Stata 实例

### 4.1 hettreatreg 命令简介

`hettreatreg`可用于处理效应异质时的线性估计。初次使用，需要通过`ssc install hettreatreg, all`以安装外部命令。利用`help hettreatreg`，获得其语法如下：

```stata
hettreatreg indepvars [if] [in], outcome(varname) treatment(varname) [options]
```

- `indepvars`:解释变量，可搭配`if`、`in`等条件语句。
- `outcome()`:设定一个结果变量
- `treatment()`:设定一个二元干预变量
- `options`:包括`noisily`，展示模式估计过程的输出结果；以及`vce(vcetype)`，其中，`vcetype`可为`ols`、`robust`、`cluster`、`bootstrap`、`jackknife`、`hc2`、`hc3`等。
  &emsp;

我们使用的是 Stata 自带数据集 nswcps.dta 。基于该数据集，我们来进行 Stata 实操。

首先我们用`regress`命令进行一次估计。

```stata
 regress re78 treated age-re75,vce(robust)
 Linear regression                               Number of obs     =     16,177
                                                F(10, 16166)      =    1718.20
                                                Prob > F          =     0.0000
                                                R-squared         =     0.4762
                                                Root MSE          =     7001.7

------------------------------------------------------------------------------
             |               Robust
        re78 |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
     treated |    793.587   618.6092     1.28   0.200    -418.9555     2006.13
         age |  -233.6775    40.7162    -5.74   0.000    -313.4857   -153.8692
        age2 |   1.814371   .5581946     3.25   0.001     .7202474    2.908494
        educ |   166.8492   28.70683     5.81   0.000     110.5807    223.1178
       black |  -790.6086   197.8149    -4.00   0.000    -1178.348   -402.8694
    hispanic |  -175.9751   218.3033    -0.81   0.420    -603.8738    251.9235
     married |    224.266   152.4363     1.47   0.141    -74.52594    523.0579
    nodegree |   311.8445    176.414     1.77   0.077     -33.9464    657.6355
        re74 |   .2953363   .0152084    19.42   0.000     .2655261    .3251466
        re75 |   .4706353   .0153101    30.74   0.000     .4406259    .5006447
       _cons |   7634.344   737.8143    10.35   0.000     6188.146    9080.542
------------------------------------------------------------------------------

```

然后我们再使用`hettreatreg`命令进行一次相同的估计。

```stata
 hettreatreg age-re75 , outcome(re78) treatment(treated) noisily vce(robust)
 Linear regression                               Number of obs     =     16,177
                                                F(10, 16166)      =    1718.20
                                                Prob > F          =     0.0000
                                                R-squared         =     0.4762
                                                Root MSE          =     7001.7

------------------------------------------------------------------------------
             |               Robust
        re78 |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
     treated |    793.587   618.6092     1.28   0.200    -418.9555     2006.13
         age |  -233.6775    40.7162    -5.74   0.000    -313.4857   -153.8692
        age2 |   1.814371   .5581946     3.25   0.001     .7202474    2.908494
        educ |   166.8492   28.70683     5.81   0.000     110.5807    223.1178
       black |  -790.6086   197.8149    -4.00   0.000    -1178.348   -402.8694
    hispanic |  -175.9751   218.3033    -0.81   0.420    -603.8738    251.9235
     married |    224.266   152.4363     1.47   0.141    -74.52594    523.0579
    nodegree |   311.8445    176.414     1.77   0.077     -33.9464    657.6355
        re74 |   .2953363   .0152084    19.42   0.000     .2655261    .3251466
        re75 |   .4706353   .0153101    30.74   0.000     .4406259    .5006447
       _cons |   7634.344   737.8143    10.35   0.000     6188.146    9080.542
------------------------------------------------------------------------------

"OLS" is the estimated regression coefficient on treated.

   OLS  =  793.6

P(d=1)  =  .011
P(d=0)  =  .989

    w1  =  .983
    w0  =  .017
 delta  =  -.971

   ATE  =  -6751
   ATT  =  928.4
   ATU  =  -6840

OLS = w1*ATT + w0*ATU = 793.6
```

可以看出，`hettreatreg`估计出的系数与`regress`是相同的，只不过`hettreatreg`报告出了更多的结果。其中， ols 为干预变量的回归系数， $P( d=1 )$ 与 $P( d=0 )$ 分别是干预组与对照组的样本占比。 $\omega_{1}$ 与 $\omega_{1}$ 分别是 ATT 与 ATU 的 OLS 权重。 $\delta$ 则被用于估计 ATE 。 &emsp;

### 4.2 结果复现

NSW-CPS 数据包含 1975 年 12 月进入 NSW 项目、1978 年离开 NSW 项目随机化实验的男女性。因为个体是 1975 年 12 月进入实验，因而，1974 年和 1975 年的收入是干预前变量，没有收到培训的影响。1978 年的收入是参与 NSW 项目培训过后的结果，收到 NSW 项目培训的影响。基于该数据集，我们利用 OLS 来进行结果复现。&emsp;

回归模型为：
$$y=\alpha +\tau d+ X\beta+ u $$
其中，$y$ 为 1978 年的收入，$d$ 为干预变量，$X$ 为一系列协变量，包括 1974 年收入、1975 年收入、年龄、受教育程度等等。&emsp;

回归结果复现代码和结果如下：

```stata
 sysuse nswcps,clear  //针对3.1的引例，导入NSW-CPS数据库
 
 count if treated==1 //计算干预组个体数量
 scalar N_treated = r(N)
 count if treated ==0 //计算控制组个体数量
 scalar N_untreated = r(N)
 scalar rho_hat=N_treated/(N_treated+N_untreated) //生成rho的估计值，即干预组个体在总体的占比
 scalar w0_star=rho_hat //生成w0_star估计值
 scalar delta_star=2*rho_hat-1 //生成delta_star估计值
 
 hettreatreg re75 , outcome(re78) treatment(treated) noisily vce(robust) //仅将1975年的收入作为解释变量进行回归
 hettreatreg age-nodegree , outcome(re78) treatment(treated) noisily vce(robust) //不考虑1974年与1975年的收入进行回归
 hettreatreg age-nodegree re75 , outcome(re78) treatment(treated) noisily vce(robust) //不考虑1974年的收入进行回归
 hettreatreg age-re75 , outcome(re78) treatment(treated) noisily vce(robust) //回归时所有协变量均被包括
```
&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/用OLS估计异质性处理效应_Fig1_NSW_林旭姿.png)
&emsp;

根据回归结果，我们可以看到，对于任何一种情况而言，$\widehat{\omega}_{0}$ 均介于 0.1% 与 0.9% 之间；相似的，$\widehat{\omega}^*_{0}$ 总是等于被干预个体在总体的占比 ( 该样本中仅为 1.1% )。这表明 ATT 的权重接近于 1 ，而 ATU 的权重接近于 0 。&emsp;

在表格的第 ( 4 ) 列中，$\widehat{\omega}_{0}=0.017$，$\widehat{\delta}=-0.971$。这表明了：如果我们的目标是估计 $\tau_{ATT}$，使用 OLS 模型估计模型 ( 1 ) 会使得 OLS 估计量 $\tau$ 偏离 $\tau_{ATT}$ 约 $(\tau_{ATU}-\tau_{ATT})$ 的 1.7%，该结论可由推论 3 得出。如果我们想要直接用 $\tau$ 来解释 $\tau_{ATE}$ ，则估计量则会偏离 $\tau_{ATE}$ 约 $(\tau_{ATU}-\tau_{ATT})$ 的 97.1%，该结论可由推论 2 得到。由表也可看出，在$\widehat{\delta}$的绝对值接近于 1 的情况下， OLS 估计量 ( 794 美元) 与$\widehat{ATE}$ ( $-6840$ 美元 ) 的差异是巨大的。在本例中，我们估计出的 OLS 估计量与 ATT 间的差异比 ATU 与 ATT 间差异的百分之二还要小。因此，在本例中，$\tau$ 应当被解释为 $\tau_{ATT}$，而非 $\tau_{ATE}$。&emsp;

从表中我们还可以看出，当 ATT 的估计为 928 美元时，ATU 的估计为 $-6840$ 美元。换句话说，OLS 估计量，即 794 美元，是这两个数字的加权平均，且与 ATT 估计值 928 美元十分接近。这是基于极小比例的被干预的变量 ( 仅仅 1.1% )而得到的结果。ATT 的估计量 928 美元的权重 ，$\widehat{\omega}^*_{1}$ 为 98.3%，而 ATU 的估计量 -6840 美元的权重 ，$\widehat{\omega}^*_{0}$ 仅为 1.7%。&emsp;

最后，根据该表，我们便可以写出 $\widehat{\tau}=\widehat{\omega}_{ATT} \cdot\widehat{\tau}_{ATT}+(1-\widehat{\omega}_{ATT})\cdot\widehat{\tau}_{ATU}$，解出 $\widehat{\omega^*}_{ATT}$ 的值为 98.3% ，这正好与对照组个体的占比 98.9% 十分接近。

## 5 结论

Tymon Sloczynski 的研究为控制效应异质时的 OLS 估计量提出了一个新的解释。其研究的主要结论为：OLS 估计量 $\tau$ 实质上是两个参数的凸组合 ( convex combination ) 。在特定条件下，这两个参数可以被解释为干预组的平均处理效应 ( ATT ) 与对照组的平均处理效应 ( ATU ) 。除此之外，$\tau$ 是这两个参数的加权平均，其各自权重分别与每个组别的观测值在总体里的占比反向相关，这将导致用 OLS 估计量来解释 ATE 或 ATT 时产生偏误。

## 6 参考文献
- Słoczyński, Tymon. 2020. “Interpreting OLS Estimands When Treatment Effects Are Heterogeneous: Smaller Groups Get Larger Weights.” 2021 Stata Conference 11, Stata Users Group. The Review of Economics and Statistics, 1–27. [-PDF-](https://arxiv.org/pdf/1810.01576.pdf)

- LaLonde, R. J. (1986). Evaluating the econometric evaluations of training programs with experimental data. American Economic Review, 76:604–620.

- 赵西亮. 基本有用的计量经济学.北京大学出版社,2017


