IV 和 OLS 估计系数差异之分解

&emsp;

> **作者：** 蔡金威（中山大学）        
> **E-mail:**  <caij66@mail2.sysu.edu.cn>   

> 本文主要摘译自下文，特此致谢！   
> **Source**：Ishimaru, Shoya. 2021. Empirical Decomposition of the IV-OLS gap with Heterogeneous and Nonlinear Effects. arXiv:2101.04346. [-Link-](https://arxiv.53yu.com/pdf/2101.04346.pdf)  

---

# 目录

[toc]

## 1. 引言

工具变量回归是估计潜在的内生变量因果效应的最常见方法。计量经济学中将 IV 回归和普通最小二乘法 （OLS） 对 **X** 的系数估计之间的差距解释为与遗漏变量、选择误差、测量误差和双向因果有关的内生性偏差结果。如果这些解释不能为 IV-OLS 系数差距提供一个合理解释时，实证研究者会考虑会有这样一种可能性，回归方程为线性预测模型，并且允许处理水平在真正的因果关系中具有异质性和非线性。IV-OLS 系数差距是 IV 和 OLS 系数对不同的处理边际和个人群体给予不同的权重形成的。

本文介绍基于 Shoya Ishimaru  （2021） 提出的具有异质性和非线性的 IV-OLS 系数差距的实证分解框架。作者指出大部分文献都是在没有协变量或者固定协变量的单变量模型上做分解，而他受文献发展中的加权平均解释 （Lochner and Moretti，2015） 启发，将 IV-OLS 系数差距实证分解为处理水平 **X** 上的权重差，协变量 **W** 上的权重差和内生性偏差部分。

## 2. 理论基础

### 2.1 模型和条件

首先作者随机抽取 （Y，X，W，Z） 其中**Y**是一个标量结果变量，**X** 是一个标量处理变量，**W** 是一个协变量向量，**Z** 是一个标量工具变量。为了评估变量 **X** 对结果变量 **Y** 的影响，指定一个线性回归模型

$$Y=\beta X+W'\gamma+\varepsilon ，E(\varepsilon W)=0 \quad (1)$$

附加矩条件 $E(\varepsilon Z)=0$ 和 $E(\varepsilon X)=0$ 后，分别将线性 IV 系数 $\beta_{IV}$ 和 OlS 系数 $\beta_{OLS}$ 表示为

$$\beta_{IV}=E({\widetilde Y\widetilde Z})/E({\widetilde X\widetilde Z}) \quad (2)$$
$$\beta_{OLS}=E({\widetilde Y\widetilde Z})/E({\widetilde X}^2) \quad (3)$$

然而上述的线性回归模型仅为 IV 与 OLS 系数的统计描述，并没有包含潜在 IV 和 OLS 系数的因果关系。为了解释潜在 IV 和 OLS 系数的因果关系，作者令 **X** 为 **Y** 的内生性变量，即 $Y=Y(X)$ 。同时令该模型满足下列假设：1.模型满足多元线性回归的基本条件。2.工具变量 **Z** 具有外生性和相关性。3.条件平均数 $E(Z|W)$ 和 $E(X|W)$ 是线性的。4.变量 **X** 在模型中是连续和可微的。

在上述假设的背景下，作者展示了 IV 与 OLS 系数的加权平均解释：

### 2.2 定理

**定理1.** IV 和 OLS 系数的加权平均解释。
$$\beta_{IV}=\int \int\frac\partial{\partial x}g(x,w)w_Z(x,w)dF_W(w)dx\quad (4)$$
$$\beta_{OLS}=\int\int\frac\partial{\partial x}m(x,w)w_X(x,w)dF_W(w)dx \quad (5)$$

其中，IV 系数识别的是因果关系 $\frac\partial{\partial x}g(x,w)$ 的加权平均。OLS 系数的加权平均解释援引 Angrist and Krueger（1999） 的结果，识别的是条件平均函数 $\frac\partial{\partial x}m(x,w)$ 中的加权平均斜率。OLS 识别的边际效应和 IV 识别的边际效应之间的关系 $\frac\partial{\partial x}m(x,w)$ 和 $\frac\partial{\partial x}g(x,w)$ 可以被解释为：
$$\frac\partial{\partial x}m(x,w)=\frac\partial{\partial x}g(x,w)+\frac\partial{\partial x}E[U|X=x,W=w]\quad (6)$$

两个边际效应的差距来源于内生性偏差，即处理变量 **x** 和不可观测项 **U** 之间的关系，由此我们可以得到第一个IV-OLS系数差距的来源。然而，权重函数 $w_Z$ 和 $w_X$ 同样也是造成 IV-OLS 系数差距的原因，为了探究权重函数对 IV-OLS 系数所造成的差距。我们在协变量 $W=w$ 时，令 $\overline w_R(w)=\int w_R(x,w)dx$ ，$(R=Z,W)$ 。边际 IV 和 OLS 权重可以被解释为:
$$\overline w_Z(w)=Cov(X,Z|W=w)/E(\widetilde X\widetilde Z)\quad (7)$$ $$\overline w_X(w)=Var(X|W=w)/E(\widetilde X^2)\quad (8)$$


在 $W=w$ 时，IV 和 OLS 系数可以表示为：
$$b_{IV}(w)=Cov(Y,Z|W=w)/Cov(X,Z|W=w) \quad (9)$$
$$b_{OLS}(w)=Cov(Y,X|W=w)/Var(X|W=w) \quad (10)$$


援引 Lochner 和 Moretti （2015） 的结果，IV 和 OLS 系数 $\beta_{IV}$ 和 $\beta_{OLS}$ 可以被看成是特定协变量系数 $b_{IV}$ 和 $b_{OLS}$ 的加权平均数。
如下所示：

**定理2.** IV 和 OLS 系数在特定协变量系数下的加权平均解释 （Lochner and Moretti，2015）。
$$\beta_{IV}=\int b_{IV}(w)\overline w_Z(w)dF_W(w) \quad (11)$$
$$\beta_{OLS}=\int b_{OLS}(w)\overline w_X(w)dF_W(w) \quad (12)$$


**定理3.** 权重在处理水平 **X** 上时特定协变量系数的解释。

设 $w_R(x|w)=w_R(x,w)/\overline w_R(w)$ 为给定 $W=w$ 时处理水平 **X** 的条件权重。在上述假设以及新追加的假设 $Cov(U,Z|W=)=0$ ， $Cov(X,Z|W=w)\neq 0$ 和 $Var(X|W=w)>0$ 的情况下，特定协变量系数可以解释为：
$$b_{IV}(w)=\int\frac\partial{\partial x}g(x,w)w_Z(x|w)dx \quad (13)$$
$$b_{OLS}(w)=\int\frac\partial{\partial x}m(x,w)w_X(x|w)dx \quad (14)$$
其中， IV 系数的解释和 OLS 系数的解释分别来自 schechtman and Yitzhaki（2004）和 Yitzhaki （1996） 。此时的特定协变量系数是权重在处理水平 **X** 上时，已识别边际效应 $\frac\partial {\partial x}g(x,w)$和$\frac\partial {\partial x}m(x,w)$ 的加权平均数。


### 2.3 IV-OLS 系数差距的分解

在定理 1-3 中对 IV 和 OLS 系数的加权平均解释推动了对 IV-OLS 系数差距分解。IV-OLS 系数差距可以分解成三个可估计的部分，分别是 1. 协变量权重差异，即权重放在协变量 **W** 上的 IV 和 OLS 之间系数差异； 2. 处理水平的权重差异，权重在不同处理水平 **X** 的差异； 3. 内生性偏差（或边际效应偏差） ，是 IV 和 OLS 识别的边际效应之间的差异，源于处理水平 **X** 和不可观察项 **U** 之间的相关性。

第一个部分的差异为协变量权重差异，对应于将权重放在协变量上时 **IV** 和 **OLS** 系数差异。
$$\Delta_{CW}=\int b_{ols}(w)(\overline\omega_z(w)-\overline\omega_x(w))dF_W(w) \quad (15)$$

第二个部分是处理水平的权重差异，反映了将权重放在处理水平，同时有条件的将权重放在协变量上时的 IV 和 OLS 系数差距。
$$\Delta_{TW}=\int\int \frac\partial{\partial x}m(x,w)(\omega_Z(x|w)-\omega_X(x|w))\overline\omega_Z(w)dF_W(w)dx\quad (16)$$

第三个部分为内生性偏差或边际效应差异，反应了 IV 和 OLS 识别的边际效应之间的差异，产生于 **X** 的内生性。
$$\Delta_{ME}=\int\int (\frac\partial{\partial x}g(x,w)-\frac\partial{\partial x}m(x,w))w_Z(x,w)dF_W(w)dx\quad (17)$$


这三个部分的总和即为 IV 系数和 OLS 系数的差距，即 $\beta_{IV}-\beta_{OLS}=\Delta_{CW}+\Delta_{TW}+\Delta_{ME}$。

### 2.4 在 DID 与 RD 基础上的 IV-OLS 系数差距分解

此实证框架可以在 DID 和 RD 的基础上进行拓展，在 DID 识别策略下，等式被改写为：
$$Y=\beta X+\sum\limits_{g=1}^{G}\gamma_gd_g+\sum\limits_{t=1}^{T-1}\delta_tD_t+\varepsilon\quad (18)$$
其中每个观测项 $g\in\{1,...,G\}$，时间$t\in\{1,...,T\}$ 和工具变量 **Z** 在 $(g,t)$ 中总是不变的。

由于线性函数假设不成立，需要再添加下列假设条件：1. 线性结构函数的假设，即在 **W** 中 $g(x,w)$ 对于任意 $x\in(\underline x,\overline x)$ 都是线性的。 2. 潜在结果 $Y(x)$ 满足 $Cov(Y(x),Z|W)=0$ 的条件独立性和可微性。有了上述假设可以得到 IV 系数的加权平均解释。

**定理4.** IV 系数加权平均解释：
$$\beta_{IV}=\int\int\frac\partial{\partial x}g(x,w) w^\ast_Z(x,w)dF_W(w)dx\quad (19)$$
其中 $w^\ast_Z(x,w)=L_w(1_{X\ge x}\widetilde Z)/E(\widetilde X\widetilde Z)$ 满足 $\int\int w^\ast_Z(x,w)dF_W(w)dx=1$ ，并且在协变量满足 $W=w$ 和处理水平满足 $X=x$ 的条件时有以下结果：
$$\overline w^\ast_Z(w)=\int w^\ast_Z(x,w)dx=L_w(X\widetilde Z)/E(X\widetilde Z)\quad (20)$$
$$\overline w^\ast_Z(x)=\int w^\ast_Z(x,w)dF_W(w)=E(1_{X\ge x}\widetilde Z)/E(X\widetilde Z)\quad (21)$$

在给定条件独立性和可微性的条件后，可以得到定理5。

**定理5.** IV 系数是因果效应 $Y^{\prime}(x)$ 的加权平均数 
$$\beta _{IV}=\int\int\tau_{IV}(x,w)w_Z(x,w)dF_W(w)dx\quad (22)$$
IV 识别边际效果 $\tau_{IV}(x,w)=E[Y^{\prime}(x)\lambda(Y^{\prime}(x)|x,w)|W=w]$ 。

基于定理 4 、定理 5 和加权平均解释，内生性偏差分解仍然是有效的，如下所示：
$$\Delta_{ME}=\int\int(\tau(x,w)-\frac\partial{\partial x}m(x,w))w_Z(x,w)dF_W(w)dx+\int\int(\tau_{IV}(x,w)-\tau(x,w))w_Z(x,w)dF_W(w)dx\quad (23)$$
只是这个分解式不再单独代表内生性偏差，第一项通过平均边际效应 AME $\tau(x,w)$ 差异衡量了内生性偏差，第二项则代表了不可观测项驱动的权重差异。尽管内生性偏差和不可观测项驱动的权重差异并不能单独识别，但是总的来说仍然好于直接观测 IV-OLS 系数差距。

## 3. Stata 应用

下面将介绍基于上述理论的 Stata 应用，即外部命令 `ivolsdec` ，该命令适用于解释结果变量 **Y** 和处理变量 **X** 之间关系的非线性和观察到的异质性，允许 **X** 对 **Y** 的真正因果影响在 **X** 中是非线性的，在 **W** 中是异质性的，并评估 IV-OLS 系数差距如何受到 IV 和 OLS 估计中 **X** 和 **W** 权重的影响。

首先利用 `ssc install ivolsdec` 下载外部命令。

该命令的语法为：

```
ivolsdec outcome (treatment=instruments) covariates [weight] [if exp] , xnbasis(varlist) [wbasis(varlist) xibasis(varlist) vce(vcetype) didrdd format(fmt)]
```

*`outcome`:结果变量

*`treatment`:处理变量

*`instruments`:工具变量（可有多个工具变量）

*`covariates`:协变量（可有一系列协变量）

*`weight`:权重（四种权重：分别为fweight、pweight、aweight、iweight）

*`xnbasis`：指定了未与wbasis交互的X的基函数。

*`wbasis`： 指定与X和xibasis交互的W的基函数。如果没有指定，将使用协变量中的变量。

*`xibasis`： 指定了与wbasis交互作用的X的基函数。


*`vce`：指定报告的标准误差的类型。vcetype接受regress和ivregress命令中的标准选项，例如（cluster clustvar）。

*`did`：表示原始的IV回归在工具变量中使用DID变化进行识别。

*`rdd`：表示原始的IV回归实现了（模糊）不连续回归设计。

*`format`：指定结果的显示格式。

下面我们用一个调查的女性工资数据来检验此命令。下列数据为 Stata 官方在介绍 `ivolsdec` 命令时所使用的来自于 Load Card （1995） 的外部数据集。

```
use http://www.stata.com/data/jwooldridge/eacsap/card, clear

/*    Notes:
数据中每个个体为一名女性，包括了其工资，以及其他一些人口地理学特征。
其中一部分女性的工资为缺失值，意味着未进入劳动力市场。

   educ:教育年限
   age:年龄
   black:=1为黑人
   smsa66:=1为1966年在大都市
   smsa：=1为1976年在大都市
   south66:=1为1966年在南部城市
   sinmom14:=1为14岁时跟随单亲母亲生活
   kww:工作知识面
   nearc4:=1为有接近四年的大学学历
   educ_c:受高等教育的年限
*/
```

下面为利用 `regress` 和 `ivregress` 命令估计的 OLS 系数与 IV 系数（ivolsdec 命令不用提前算 OLS 系数与 IV 系数）

```
. regress lwage educ age black smsa66 south66 sinmom14 kww, robust

Linear regression                               Number of obs     =      2,963
                                                F(7, 2955)        =     162.97
                                                Prob > F          =     0.0000
                                                R-squared         =     0.2680
                                                Root MSE          =     .37809

------------------------------------------------------------------------------
             |               Robust
       lwage |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
        educ |    .025619   .0031166     8.22   0.000     .0195081      .03173
         age |    .029994   .0025604    11.71   0.000     .0249737    .0350144
       black |  -.1171977   .0202054    -5.80   0.000    -.1568158   -.0775796
      smsa66 |   .1010268   .0149897     6.74   0.000     .0716354    .1304182
     south66 |   -.091606   .0160861    -5.69   0.000    -.1231471   -.0600649
    sinmom14 |  -.0176363   .0229406    -0.77   0.442    -.0626174    .0273448
         kww |   .0087473   .0011285     7.75   0.000     .0065346    .0109601
       _cons |   4.787954   .0785691    60.94   0.000     4.633899     4.94201
------------------------------------------------------------------------------
```

```
. ivregress 2sls lwage (educ=nearc4) age black smsa66 south66 sinmom14 kww, robust

Instrumental variables (2SLS) regression          Number of obs   =      2,963
                                                  Wald chi2(7)    =     874.29
                                                  Prob > chi2     =     0.0000
                                                  R-squared       =     0.0805
                                                  Root MSE        =      .4232

------------------------------------------------------------------------------
             |               Robust
       lwage |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
        educ |   .1108425   .0659555     1.68   0.093     -.018428     .240113
         age |   .0448982   .0117425     3.82   0.000     .0218833    .0679132
       black |  -.1225513   .0227123    -5.40   0.000    -.1670666    -.078036
      smsa66 |   .0874981   .0197614     4.43   0.000     .0487665    .1262297
     south66 |  -.0624463    .028879    -2.16   0.031    -.1190481   -.0058445
    sinmom14 |   .0355491    .048747     0.73   0.466    -.0599932    .1310915
         kww |  -.0053318   .0109385    -0.49   0.626    -.0267709    .0161073
       _cons |    3.70112   .8410937     4.40   0.000     2.052606    5.349633
------------------------------------------------------------------------------
Instrumented:  educ
Instruments:   age black smsa66 south66 sinmom14 kww nearc4
```

### 3.1 IV-OLS 系数差距分解

```
. ivolsdec lwage (educ=nearc4) age black smsa66 south66 sinmom14 kww, xnb(i.educ) format(%7.3f)
 Outcome (Y): lwage
 Treatment (X): educ
 Instruments (Z): nearc4
 Covariates (W): age black smsa66 south66 sinmom14 kww

辅助回归的基函数规定如下
 p(X): i.educ \\
 q(W): age black smsa66 south66 sinmom14 kww
 r(X): 

Decomposition Results

                    |    Coef   StdErr 
--------------------+------------------
                OLS |   0.026    0.003 
                 IV |   0.111    0.066 
         IV-OLS Gap |   0.085    0.066 
   Covariate Weight |  -0.004    0.008 
Treatment-level Wgt |  -0.007    0.008 
    Marginal Effect |   0.096    0.067 

Number of Observations: 2963
VCE Type: robust
```

分解结果表格如上，其中第一行为 OLS 系数和标准误，第二行为 IV 系数和标准误，第三行为 IV-OLS 系数差距和标准误，第四行为协变量权重差距和标准误，第五行为处理水平权重差距和标准误，第六行为边际效应差距和标准误。其中， OLS 和 IV 估计的系数和标准误直接来源于 `regress` 和 `ivregress` 命令中 **X** 的系数和标准误，分解估计的标准误则利用 `regress` 命令计算得到。从表中结果得知， IV-OLS 系数差距= IV 系数- OLS 系数，即为协变量权重差距、处理水平权重差距和边际效应差距的和，系数差距为 0.085。 

另外，在计算系数分解时，此命令执行以下两个辅助 OLS 回归：
$Y=W'a1+(q(W)'b1)*X+e1$ 和

$Y=W'a2+(q(W)'b2)*X+q(W)'(C2)r(X)+p(X)'d2+e2$

其中 $q(W)$ 是 `wbasis` 指定的一组 **W** 的基函数 （默认值为 $q(W)=W$ ) ， $r(X)$ 是 `xibasis` 指定的一组 **X** 的基函数 （默认值为 $r(X)=0$ ）， $p(X)$ 是 `xnbasis` 指定的一组 **X** 的基函数（必须由用户选择）。


### 3.2 指定 xibasis 的系数差距分解

下列回归将尝试把 $r(x)$ 指定为 **educ_c** ，即指定 $r(x)$ 为受高等教育年限。

```
. gen educ_c=max(educ-12,0)
. ivolsdec lwage (educ=nearc4) age black smsa66 south66 sinmom14 kww, xnb(i.educ) xib(educ_c) format(%7.3f)
 Outcome (Y): lwage
 Treatment (X): educ
 Instruments (Z): nearc4
 Covariates (W): age black smsa66 south66 sinmom14 kww

辅助回归的基函数规定如下
 p(X): i.educ
 q(W): age black smsa66 south66 sinmom14 kww
 r(X): educ_c
 

Decomposition Results

                    |    Coef   StdErr 
--------------------+------------------
                OLS |   0.026    0.003 
                 IV |   0.111    0.066 
         IV-OLS Gap |   0.085    0.066 
   Covariate Weight |  -0.004    0.008 
Treatment-level Wgt |  -0.007    0.010 
    Marginal Effect |   0.096    0.067 

Number of Observations: 2963
VCE Type: robust
```

我们可以看到除了处理水平权重的标准误从 0.008 提高到 0.010 ，其他结果与 $r(x)$ 为 0 的分解结果并无二致。可以看出， $r(x)$ 的指定与否并不会对分解结果有太大影响。有趣的一点是，在指定 $r(x)$ 为 **educ_c** 之后，分解结果的标准误反而会提高，导致可靠度降低，这暗示我们并不用一味地寻求基函数来降低分解结果的标准误。

### 3.3 DID 设定下的系数差距分解

下面将尝试在 DID 的设定下分解 IV 和 OLS 系数的差距。

```
. gen smsa6=smsa*smsa66 \\smsa和smsa66的交乘项
. ivolsdec lwage (educ=nearc4) age black smsa66 south66 smsou6 sinmom14 kww, xnb(i.educ) rdd format(%7.3f)
 Outcome (Y): lwage
 Treatment (X): educ
 Instruments (Z): nearc4 (RD-type)
 Covariates (W): age black smsa66 south66 smsou66 sinmom14 kww

 辅助回归的基函数规定如下
 p(X): i.educ
 q(W): age black smsa66 south66 smsou66 sinmom14 kww
 r(X): 
 

Decomposition Results

                    |    Coef   StdErr 
--------------------+------------------
                OLS |   0.026    0.003 
                 IV |   0.111    0.066 
         IV-OLS Gap |   0.085    0.066 
   Covariate Weight |   0.008    0.009 
Treatment-level Wgt |  -0.007    0.008 
    Marginal Effect |   0.085    0.065 

Number of Observations: 2963
VCE Type: robust
```

在 DID 的设定下，IV-OLS 系数差距的分解结果不再是协变量权重差异、处理水平权重差异和边际效应差异的和， IV-OLS 系数差距 = 边际效应差距，即0.085，与上述理论分析中关于 DID 基础上的 IV-OLS 系数差距的结论相符。

## 4. 总结

当 IV 与 OLS 估计值不同时，传统的方法可以考虑两种解释，第一种是按字面意思理解线性回归方程，将系数差距解释为内生性偏差。第二种是将系数差距解释为权重差。如果想要更进一步同时量化系数差距的权重差和内生性偏差，则可以采用 Shoya Ishimaru （2021） 在此基础上提出的分解框架和 Stata 应用 `ivolsdec` 。该方法能够将 IV-OLS 系数差距同时分解为协变量的权重差、处理水平的权重差和被识别的边际效应差，然而，在 DID 设定下 IV-OLS 系数差距的分别识别还有待改进。

## 5. 参考文献


- Lochner, L. and E. Moretti. 2015. Estimating and Testing Models with Many Treatment Levels and Limited Instruments. Review of Economics and Statistics 97: 387-397. [-Link-](https://www.nber.org/system/files/working_papers/w17039/w17039.pdf)

- Ishimaru, Shoya. 2021. Empirical Decomposition of the IV-OLS gap with Heterogeneous and Nonlinear Effects. arXiv:2101.04346. [-Link-](https://arxiv.53yu.com/pdf/2101.04346.pdf)

- Schechtman, E., & Yitzhaki, S. 2004. The Gini Instrumental Variable, or the “double instrumental variable” estimator. Metron, 287-313.[-Link-](https://www.researchgate.net/publication/279897993_The_Gini_Instrumental_Variable_or_the_double_instrumental_variable_estimator)

- Yitzhaki, S. 1996. On using linear regressions in welfare economics. Journal of Business & Economic Statistics, 14(4), 478-486.[-Link-](http://jenni.uchicago.edu/underiv/Yitzhaki_HebU_wp217_1989.pdf)

- Angrist, J. D., Krueger, A. B. 1999. Empirical strategies in labor economics. Handbook ofLabor Economics. 3, 1277–1366.[-Link-](https://dspace.mit.edu/bitstream/handle/1721.1/63521/empiricalstrateg00angr.pdf?sequence=1)

