# 论文复现：Did Protestantism promote prosperity via higher human capital? Replicating the Becker–Woessmann (2009) results

 

> **论文作者**：Jeremy Edwards
>
> **推文作者：** 甘展豪 (中山大学)      
>
> **E-mail:**  <ganzhh3@mail2.sysu.edu.cn>   

> **Source:** Jeremy Edwards, 2021. "Did Protestantism promote prosperity via higher human capital? Replicating the Becker–Woessmann (2009) results" [-PDF-](https://onlinelibrary.wiley.com/doi/epdf/10.1002/jae.2851)



### 目录

[TOC]

## 1、引言

本文是对文章 Becker, S. O., & Woessmann, L.  (2009) . Was Weber wrong? A human capital theory of Protestant economic history.  的复现、讨论与修正。

BW (2009) 使用了来自 19 世纪晚期普鲁士的数据，认为新教由于强调教育而促进了繁荣，因为当时新教相信每个人都应该接受教育从而能阅读圣经，新教徒比天主教徒拥有更多的人力资本，因此新教能促进繁荣。

在模型中，由于存在内生性，BW 使用的工具变量是与维滕贝格的距离，是一个空间变量，维滕贝格是新教改革开始的城市。但 19 世纪普鲁士不同地区存在制度差异以及区域效应影响繁荣。BW 使用此工具变量时，没有考虑这些影响因素，因此文章结论可能出错。

 

## 2、对BW论文的复现

BW 的识别新教的影响分位三个步骤：

1、使用  IV  估计来表明新教对识字率有积极的因果影响，识字率是衡量人力资本的标准。

2、使用  IV  估计来论证新教对繁荣的总体积极影响。

3、通过  IV  估计来表明控制识字率后，新教对繁荣几乎没有影响，从而说明新教是通过提高人力资本来促进繁荣的。

本文首先复现的是 BW 的第二步，即估计新教对繁荣的总体影响。


$$
Y=\alpha+\beta_1Prot+\beta X+\epsilon \tag{1}
$$


BW 估计新教对繁荣的总影响时，选取人均所得税 (Income tax per capita) 、小学教师收入的对数 (Log teacher income) 以及非农劳动力份额 (Non-agricultural share) 作为繁荣 Y 的代理变量，选取新教徒的份额 Prot 作为自变量，选取人口统计学因素作为控制变量。由于存在内生性问题， BW 选取了与维滕贝格的距离作为工具变量。

本文使用 BW 的数据集，分别对 3 个因变量进行 IV 估计和 OLS 估计。

以下以人均所得税因变量为例展示回归结果： **inctaxpc** 为人均所得税收入， **f_prot**  为新教徒份额， **kmwittenberg** 是到与维滕贝格的距离，其他变量为控制变量。



```Stata
ivreg2 inctaxpc f_young f_jew f_fem f_ortsgeb f_pruss hhsize lnpop gpop f_blind f_deaf f_dumb (f_prot = kmwittenberg), small cluster(rbkey)


------------------------------------------------------------------------------
             |               Robust
    inctaxpc |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      f_prot |   .5864946   .5871466     1.00   0.325    -.6067308     1.77972
              
```



并在回归后进行了 Moran Test ，莫兰检验是空间随机性的检验，原假设是空间不相关。



```Stata
predict resid if e(sample), r
moransi resid, lat( lat_proper) lon( lon_proper) swm(exp 0.03) dist(.) dunit(km) approx nomat

Moran's I Statistic                                          Number of Obs =      426
-------------------------------------------------------------------------------------
            Variable |  Moran's I         E(I)        SE(I)         Z(I)      p-value
---------------------+---------------------------------------------------------------
               resid |    0.00181     -0.00235      0.00063      6.65871      0.00000
-------------------------------------------------------------------------------------
Null Hypothesis: Spatial Randomization

```



下表第 1-2 列展示了对三个繁荣的代理变量分别回归的结果



TABLE 1. The total effect of Protestantism on county prosperity in Prussia

A. Dependent variable: Income tax per capita

| Estimation method               | IV                  | OLS                 | IV                   | OLS                 |
| ------------------------------- | ------------------- | ------------------- | -------------------- | ------------------- |
|                                 | 1.1                 | 1.2                 | 1.3                  | 1.4                 |
| Share of Protestants            | 0.586 [−0.55, 2.56] | 0.154 [−0.14, 0.44] | −0.351 [−2.10, 1.02] | 0.009 [−0.19, 0.21] |
| Elasticity                      | 0.190               | 0.050               | −0.114               | 0.003               |
| District effects                | No                  | No                  | Yes                  | Yes                 |
| Year Prussian                   | No                  | No                  | Yes                  | Yes                 |
| Adjusted *R*2                   | 0.271               | 0.309               | 0.594                | 0.604               |
| *F* statistic                   | 10.515              | –                   | 13.469               | –                   |
| *p*-value of Moran test         | 0.000               | 0.000               | 0.311                | 0.302               |
| *p*-value of test of exogeneity | 0.381               | –                   | 0.519                | –                   |

B. Dependent variable: Log teacher income

| Estimation method       | IV                  | OLS                | IV                 | OLS                |
| :---------------------- | :------------------ | :----------------- | :----------------- | :----------------- |
|                         | 1.5                 | 1.6                | 1.7                | 1.8                |
| Share of Protestants    | 0.105 [−0.09, 0.39] | 0.063 [0.01, 0.11] | 0.309 [0.04, 0.65] | 0.101 [0.04, 0.16] |
| Elasticity              | 0.067               | 0.041              | 0.199              | 0.065              |
| District effects        | No                  | No                 | Yes                | Yes                |
| Year Prussian           | No                  | No                 | Yes                | Yes                |
| Adjusted *R*2           | 0.516               | 0.521              | 0.677              | 0.728              |
| *F* statistic           | 9.555               | –                  | 10.580             | –                  |
| *p*-value of Moran test | 0.000               | 0.000              | 0.258              | 0.268              |

C. Dependent variable: Non-agricultural share

| Estimation method       | IV                  | OLS                  | IV                  | OLS                 |
| :---------------------- | :------------------ | :------------------- | :------------------ | :------------------ |
|                         | 1.9                 | 1.10                 | 1.11                | 1.12                |
| Share of Protestants    | 0.082 [−0.07, 0.26] | 0.035 [−0.004, 0.07] | 0.182 [−0.14, 0.51] | 0.058 [−0.01, 0.13] |
| Elasticity              | 0.155               | 0.067                | 0.344               | 0.110               |
| District effects        | No                  | No                   | Yes                 | Yes                 |
| Year Prussian           | No                  | No                   | Yes                 | Yes                 |
| Adjusted *R*2           | 0.591               | 0.600                | 0.725               | 0.750               |
| *F* statistic           | 9.555               | –                    | 10.580              | –                   |
| *p*-value of Moran test | 0.000               | 0.000                | 0.403               | 0.418               |



可以看到，在第 1-2 列的回归结果中，所有的莫兰检验的 p 值都非常小，拒绝了空间随机性的原假设，莫兰检验还说明空间正相关，即附近的地区有相似的误差。这些都说明了区域效应的存在。表1的结果已经是在假设误差按地区聚集的基础上估计的。

由于F统计量较小，存在弱 IV 问题，以上估计的置信区间都经过弱IV稳健处理。

总而言之，由于未考虑到的空间相关性，BW 中新教对繁荣的总体影响的估计已经变得不可靠。



## 3、新教对普鲁士繁荣的影响

### 3.1 考虑区域效应和制度影响下估计新教对普鲁士繁荣的总影响

如何解决以上问题？

在附录中，作者讨论了影响繁荣的因素。BW 认为地区之间制度差异是不重要的。然而事实上这种制度差异大大影响了普鲁士经济的发展。这种制度差异的影响可以由该地区归属于普鲁士的年份所代理，即加入普鲁士的时间长短影响了该地区制度的变更，从而影响繁荣。所以作者加入了该地区加入普鲁士时间的线性样条以便更好的拟合。该样条开始于 1525，并在 1742 和 1815 设置节点。因为普鲁士在 1742 年开始占领新的领土，并在 1815 年大幅扩张。

新模型如下：linear spline 代表该地区加入普鲁士年份的线性样条。


$$
Y=\alpha+\beta_1Prot+\beta X+\lambda linear spline+\epsilon \tag{2}
$$



面板估计方法取决于扰动项中区域效应是否与自变量相关。如果无关，那么地区内和地区间的变化都能用于估计，经过 Mundlak 检验后，这种假设被否定，即使加入线性样条，仍然需要考虑固定效应，即只使用地区内的变化来估计模型。

综上，新教对普鲁士繁荣的影响必须考虑制度影响并使用固定效应模型。模型(1.3)回归代码如下：**zupnew** 为线性样条。



```stata
xtivreg2 inctaxpc zupnew_1741 zupnew_1814 zupnew_rest f_young f_jew f_fem f_ortsgeb f_pruss hhsize lnpop gpop f_blind f_deaf f_dumb (f_prot = kmwittenberg), fe small cluster(rbkey)
```



图1的第3-4列展示了总体回归结果，此时莫兰检验的 p 值较大，空间随机性成立。

而这产生了一个新的问题，如果仅使用区域内的变化，样本中城市到维滕贝格的距离是一个弱工具变量，在以上 4 个回归中，F 统计量都比较小。弱 IV 导致标准误比较大。

在 1.7 中，对教师总收入的弱 IV 稳健置信区间仍大于0且外生性检验表明 IV 估计是必要的，由此可以判断新教对教师收入的总体影响是积极的。

而在 1.3 和 1.11 中，因为存在内生性，OLS 估计值存在衰减偏误，以人均所得税额为因变量 IV 估计值为负， OLS 估计值为正，所以新教对人均所得税的影响不可能比 OLS 估计值更高。而相反，以非农份额为因变量 IV 估计值为正且大于 OLS 的估计值，对非农份额的影响不可能更低，所以对非农份额的影响一定是积极的，但具体大小仍不确定。

表一的结论是，考虑区域效应和加入线性样条，新教对教师收入和非农份额的影响是正的，但对人均所得税的影响是不确定且不显著的。而新教对繁荣的影响是积极的是讨论新教通过促进人力资本来促进繁荣的基础，所以在下一部分只以教师的对数收入和非农劳动力份额作为繁荣的代理变量进行讨论。



### 3.2 控制识字率的影响后，估计新教对繁荣的直接影响

在讨论识字率对繁荣的影响时，BW 首先建立了以下模型并进行 OLS 回归：**Lit** 代表识字率


$$
Y=\alpha+\beta_1Prot+\gamma Lit+\beta X+\epsilon \tag{3}
$$


在控制识字率之后，新教的系数变得非常小而且不显著。但是使用 OLS 问题是识字率也是内生的，但没有足够的工具变量可供使用，由此 BW 建立一个模型来解决这个问题：


$$
Y-\bar{\gamma}LIT=\alpha+\beta_1Prot++\beta X+\epsilon \tag{4}
$$


在 BW 中，因为 OLS 估计有偏，该模型中识字率的系数 $\bar{\gamma}$ 一般调整为比 (3) 中 OLS 的点估计高 40% 或低 40% 。

为了方便理解，以下展示，考虑区域效应和制度因素，在去除识字率的影响后，新教在对繁荣的直接影响， $\bar{\gamma}$ 影响调整为 OLS 系数 40% 以下的回归代码：

**lnyteacher** 是教师收入对数，**f_prot** 是新教徒的占比，**f_rw**是识字率，**zupnew** 是线性样条，其他变量为控制变量。



```Stata
*ols估计
xtivreg2 lnyteacher f_prot f_rw zupnew_1741 zupnew_1814 zupnew_rest f_young f_jew f_fem f_ortsgeb f_pruss hhsize lnpop gpop f_blind f_deaf f_dumb f_miss, small cluster(rbkey) fe

gen y_lower = lnyteacher - 0.6*_b[f_rw]*f_rw

*IV估计
xtivreg2 y_lower zupnew_1741 zupnew_1814 zupnew_rest f_young f_jew f_fem f_ortsgeb f_pruss hhsize lnpop gpop f_blind f_deaf f_dumb (f_prot = kmwittenberg), cluster(rbkey) small fe
```



在 BW 论文中，回归同样没有考虑区域效应和制度影响，在 OLS 回归中识字率影响的点估计非常大，在随后的 IV 估计中新教徒份额的系数非常小，即直接影响较小。所以 BW 认为除去识字率以后新教的直接影响很小，新教主要通过提高识字率即提高人力资本来促进经济繁荣。

表2展示了本文按照 BW 方法估计，考虑区域效应和制度因素，在去除识字率的影响后，新教在对繁荣的直接影响，$\bar{\gamma}$ 为 OLS 点估计高 40% 以及低 40% 。



TABLE 2. The direct effect of Protestantism on county prosperity in Prussia

A. Dependent variable: Log teacher income

Assumed effect of literacy 40% below OLS estimate: (2.1)(2.2)

Assumed effect of literacy 40% above OLS estimate: (2.3)(2.4)

| Estimation method               | IV                 | OLS                | IV                  | OLS                |
| :------------------------------ | :----------------- | :----------------- | :------------------ | :----------------- |
|                                 | 2.1                | 2.2                | 2.3                 | 2.4                |
| Share of Protestants            | 0.276 [0.01, 0.60] | 0.094 [0.04, 0.15] | 0.232 [−0.04, 0.56] | 0.085 [0.03, 0.14] |
| *F* statistic                   | 10.580             | –                  | 10.580              | –                  |
| *p*-value of test of exogeneity | 0.107              | –                  | 0.185               | –                  |

 B. Dependent variable: Non-agricultural share

Assumed effect of literacy 40% below OLS estimate: (2.5)(2.6)

Assumed effect of literacy 40% above OLS estimate: (2.7)(2.8)

| Estimation method               | IV                  | OLS                 | IV                  | OLS                 |
| :------------------------------ | :------------------ | :------------------ | :------------------ | :------------------ |
|                                 | 2.5                 | 2.6                 | 2.7                 | 2.8                 |
| Share of Protestants            | 0.127 [−0.19, 0.45] | 0.047 [−0.02, 0.12] | 0.053 [−0.28, 0.38] | 0.031 [−0.04, 0.10] |
| *F* statistic                   | 10.580              | –                   | 10.580              | –                   |
| *p*-value of test of exogeneity | 0.541               | –                   | 0.872               | –                   |



这些估计值和表一的第三和第四列的对应值差异非常小，尤其是以教师收入的对数为因变量时。在 1.7 和 2.1 中，新教徒份额的系数为 0.309 和 0.276 ，置信区间分别为 [0.04,0.65] 和 [0.01,0.6] ；在 1.8 和 2.2 中系数分别为 0.101 和 0.094 ，置信区间分别为 [0.04,0.16] 和 [0.04,0.15] ，差别都非常小，即控制识字率后，新教对繁荣的直接影响与新教对繁荣的总影响差异不大，不能支持积极的总体影响是通过提高识字率实现的结论。

虽然以非农劳动力份额为因变量的回归中系数有差异，但是仍不足以证明新教是通过提高识字率来促进繁荣的。特别是在 2.5 和 2.7 表明不需要IV估计，而 OLS 中，1.12 和 2.5 显示，系数分别为 0.058 和 0.047，置信区间分别为 [-0.01,0.13] 和 [-0.02,0.12 ]，差异不大。



## 4、结论

BW 使用与维滕贝格的距离作为新教的工具变量以确定 19 世纪晚期新教对普鲁士繁荣的影响，这是独创性的。但是 BW 未考虑到区域效应和其他异质性因素影响。本文表明当考虑到这些因素时，没有证据表明新教对人均所得税有积极的总影响，但对教师收入的对数有较大影响。同时也没有证据表明这些积极的总体影响是通过提高识字率实现的，不支持 BW 的结论。



## 5、参考文献及资料

Becker, S. O., & Woessmann, L. (2009). Was Weber wrong? A human capital theory of Protestant economic history. *Quarterly Journal of Economics*, 124, 531– 596. [-PDF-](https://academic.oup.com/qje/article/124/2/531/1905076)

本文还提供了公开的数据集及附录[-Link-](http://qed.econ.queensu.ca/jae/datasets/edwards001/)















