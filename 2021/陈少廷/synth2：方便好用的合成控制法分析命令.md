&emsp;

> **作者**：陈少廷 (中山大学岭南学院)    
> **E-Mail:** <chensht39@mail2.sysu.edu.cn>  

---

**目录**
[toc]

---
## 1. 引言
经济学家常要评估某政策或事件的效应。此政策可能实施于某国家或地区（省、州或城市）。最简单的方法是考察政策实施前后的时间序列，看所关心的结果 (outcome of interest) 如何变化。但此结果可能还受其原有变化趋势的影响，或其他同时发生的混淆性事件 (confounder) 的作用。
&emsp;
为避免这种情况带来的误差，经济学家常使用“鲁宾的反事实框架” (Rubin's counterfactual framework) ，即假想该地区如未受政策干预会怎样，并与事实上受到干预的实际数据进行对比。为此， Abdi 和 Gardeazabal (2003) 提出“合成控制法” (Synthetic Control Method) 。
&emsp;
本文将介绍实现合成控制法的便捷命令 `synth2` 。

## 2. 合成控制法原理
合成控制法 (Synthetic Control Methods) 是由 Abdi 和 Gardeazabal (2003) 提出的一种政策效果评估方法，其基本思想是：将未实施某项政策的地区进行加权后合成为一个更为良好且合理的控制组，该控制组优于主观选定的控制组，可有效克服处理组和控制组之间存在的差异问题。然后根据控制组的数据特征构建“反事实”，明确处理组和控制组在政策实施之前的相似程度，避免因对比地区差异过大而引起的误差。合成控制法能够克服在选取控制对象时出现的样本选择偏误以及政策内生性问题，目前该方法已被广泛应用。
&emsp;

因篇幅有限，详细介绍及实现过程请看以下链接：
- [合成控制法简介及代码](https://www.docin.com/p-2232525892.html)
- [合成控制法 (Synthetic Control Method) 及 Stata 实现](https://www.lianxh.cn/news/9e1bb97a57041.html)
&emsp;

更多相关资料请看本文末尾参考资料和相关推文部分。

## 3. synth2 命令介绍
`synth2`为命令`synth`的包装程序，`synth2` 提供了方便的实用程序用于自动进行安慰剂检验、稳健性检验并且可以生成一系列可视化图形。在使用 `synth2` 前需要安装 `synth` 命令。
&emsp;
相关命令还有`synth_runner`——[Synth_Runner命令：合成控制法高效实现](https://www.lianxh.cn/news/db3d2785cd5c0.html)。

### 3.1 安装命令
在 Stata 命令窗口中输入以下命令即可安装 `synth2` ：

```stata
ssc install synth, replace
ssc install synth2, replace
```
### 3.2 语法结构
`synth2`的基本语法格式如下：
```stata
synth2 depvar indepvars, trunit(#) trperiod(#) [options]

options
--------------------------------------------------------
Model
  -ctrlunit(numlist)
  -preperiod(numlist)
  -postperiod(numlist)
  -xperiod(numlist)
  -mspeperiod(numlist)
  -customV(numlist)
  -nested
  -allopt

Optimization
  -margin(real)
  -maxiter(#)
  -sigf(#)
  -bound(#)

Placebo Tests
  -placebo([{unit|unit(numlist)} period(numlist) cutoff(#_c)])

Robustness Test
  -loo

Reporting
  -frame(framename)
  -nofigure
```
具体解释如下：
- **depvar** ：因变量（outcome variable）
- **indepvars** ：预测变量（predictors）
注：**depvar** 和 **indepvars** 必须是数值变量，且不能使用缩写
&emsp;
- **trunit(#)** ：必选项，指定处理单元 （trunit 表示 treated unit），只能指定一个单元
- **trperiod(#)** ：必选项，指定干预开始的时期 （trperiod 表示 treated period），必须是一个整数，只能指定一个时间段
&emsp;

- **ctrlunit(numlist)** ：选择项，指定潜在的控制单元（即 donor pool，其中 counit 表示 control units），默认为数据集中的除处理单元以外的所有单元
- **preperiod(numlist)** ：选择项，指定干预之前的时期，默认为从时间变量中可用的最早时间点到干预发生的时间点
- **postperiod(numlist)** ：选择项，指定干预之后的时期，默认为从干预发生的时间点到时间变量中可用的最近的时间点
- **xperiod(numlist)** ：选择项，指定将预测变量（predictors）进行平均的期间，默认为干预开始之前的所有时期（the entire pre-intervention period）
- **mspeperiod(numlist)** ：选择项，指定最小化均方预测误差（MSPE）的时期，默认为干预开始之前的所有时期
- **customV(numlist)** ：选择项，提供自定义的 V 型权重，它决定了在预处理期间变量对结果的预测能力，默认为等权重
- **nested** ：选择项，表示使用嵌套的数值方法寻找最优的合成控制（推荐使用此选项），这比默认方法更费时间，但可能更精确
- **allopt** ：选择项，在使用选择项 "nested" 时，如果再加上选择项 "allopt" （即"nested allopt"），则比单独使用 "nested" 更费时间，但精确度可能更高
&emsp;

- **margin(real)** ：选择项，违反容忍度，默认为5%
- **maxiter(#)** ：选择项，最大迭代次数，默认为1000次
- **sigf(#)** ：选择项，精确度，默认为7位有效数字
- **bound(#)** ：选择项，变量边界，默认为10
&emsp;

- **placebo([{unit|unit(numlist)} period(numlist) cutoff(#_c)])** ：选择项，指定要执行的安慰剂检验的类型，若不设定则默认不执行安慰剂检验
  - **{unit|unit(numlist)}** ：指定使用预测变量中的假干预控制单元的安慰剂检验。unit 使用所有的除处理单元以外的所有单元，而 unit (numlist) 使用指定的单元。这两个选项迭代地将干预重新分配给没有实际发生干预的控制单元，并计算干预效果的 p 值，一次只能输入一个选项
  - **period(numlist)** ：指定假干预时间进行安慰剂检验。这个选项将干预时间重新分配到干预前的时间段，而实际上没有干预
  - **cutoff(#_c)** ：指定临界值 "#_c" ，若预处理假干预单元的 "MSPE" 比处理单元 "MSPE" 大 "#_c" 倍，那么该假干预单元会被舍弃。其中 "#_c" 必须是大于等于1的实数。此选项仅适用于 "unit" 或 "unit(numlist)" 已被指定。若不设定则默认不会舍弃单元
&emsp;

- **loo** ：选择项，每次排除一个非零权重的控制单元的稳健性检验。迭代地重新估计模型，在每次迭代中省略一个单元，这种灵敏度检验可以评估每一个控制单元对结果的影响程度
&emsp;

- **frame(framename)** ：选择项，创建 Stata 所需格式的数据文件， "framename" 为数据文件名，该数据文件包括反事实预测、干预效果、安慰剂检验和稳健性检验（如果有）的估计结果。若同名同格式数据文件已存在，则覆盖旧文件
- **nofigure** ：选择项，不显示结果。若不设置则默认显示评估结果、安慰剂检验和稳健性检验(如果有)的所有数据

## 4. Stata实例——以加州控烟为例
**加州控烟案例原文**——[Link](https://xueshu.baidu.com/usercenter/paper/show?paperid=1s3s0p50mm780cn0km0n0gb08w261726&site=xueshu_se)
&emsp;
**4.1背景**
1988 年 11 月美国加州通过了当代美国最大规模的控烟法（anti-tobacco legislation），并于 1989 年 1 月开始生效。该法将加州的香烟消费税 （cigarette excise tax） 提高了每包 25 美分，将所得收入专项用于控烟的教育与媒体宣传，并引发了一系列关于室内清洁空气的地方立法 （local clean indoor-air ordinances） ，比如在餐馆、封闭工作场所等禁烟。 Abadie et al. (2010) 根据美国 1970-2000 年的州际面板数据，采用合成控制法研究美国加州 1988 年 第 99 号控烟法 （Proposition 99） 的政策效果。
&emsp;
```stata
sysuse smoking      //（打开数据集）
xtset state year    //（设为面板数据）
**  panel variable:  state (strongly balanced)
**   time variable:  year, 1970 to 2000
**           delta:  1 unit (设为面板数据后的结果)
set cformat  %4.3f  //回归结果中系数的显示格式
set pformat  %4.3f  //回归结果中p值的显示格式
set sformat  %4.2f  //回归结果中se值的显示格式
```
**4.2复现结果**
导入数据，将数据设为面板数据，设置输出格式。运用 `synth2` 命令重现推文「[合成控制法 (Synthetic Control Method) 及 Stata实现](https://www.lianxh.cn/news/9e1bb97a57041.html)」基于 `synth` 命令的结果

```stata
synth2 cigsale lnincome age15to24 retprice beer cigsale(1975) ///
  cigsale(1980) cigsale(1988), trunit(3) trperiod(1989)       ///
  xperiod(1980(1)1988) nested allopt 
```
- 因变量：cigsale——人均香烟消费

- 预测变量：lnincome、age15to24、retprice、beer、cigsale(1975)、cigsale(1980)和cigsale(1988)
其中 cigsale(1975) cigsale(1980) cigsale(1988) 分别表示人均香烟消费在 1975、1980 和 1988 年的取值
- trunit(3) ：表示第 3 个州（即加州）为处理组 (实验对象)
- trperiod(1989) ：表示控烟法在 1989 年开始实施 (政策实施时点)
- xperiod(1980(1)1988) ：表示将预测变量在 1980—1988 年期间进行平均，其中 "1980(1)1988" 表示时间段始于 1980 年，以 1 年为间隔，止于 1988 年
- nested allopt ：使计算结果更加精确
&emsp;

- 输出结果表格如下
```stata
--------------------------------------------------------------------------------
 Treated Unit             : California     Treatment Time           :       1989
--------------------------------------------------------------------------------
 Mean Absolute Error      =    1.20290     Number of Control Units  =         38
 Mean Squared Error       =    3.08239     Number of Covariates     =          7
 Root Mean Squared Error  =    1.75567     R-squared                =    0.97434
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
   Covariate   |  V.weight    Treated    Synthetic Control     Average Control  
               |                        Value          Bias   Value        Bias 
---------------+----------------------------------------------------------------
      lnincome |   0.0000     10.0766      9.8588    -2.16%     9.8292    -2.45%
     age15to24 |   0.5459      0.1735      0.1735    -0.01%     0.1725    -0.59%
      retprice |   0.0174     89.4222     89.4108    -0.01%    87.2661    -2.41%
          beer |   0.0031     24.2800     24.2278    -0.21%    23.6553    -2.57%
 cigsale(1988) |   0.0049     90.1000     91.6677     1.74%   113.8237    26.33%
 cigsale(1980) |   0.0066    120.2000    120.5017     0.25%   138.0895    14.88%
 cigsale(1975) |   0.4221    127.1000    127.1112     0.01%   136.9316     7.74%
--------------------------------------------------------------------------------

--------------------------
     Unit    |    U.weight
-------------+------------
        Utah |     0.3340 
      Nevada |     0.2350 
     Montana |     0.2020 
    Colorado |     0.1610 
 Connecticut |     0.0680 
--------------------------

-----------------------------------------------------------
 Time | Actual Outcome  Predicted Outcome  Treatment Effect
------+----------------------------------------------------
 1989 |       82.4000            89.9945           -7.5945 
 1990 |       77.8000            87.5039           -9.7039 
 1991 |       68.7000            82.1751          -13.4751 
 1992 |       67.5000            81.6075          -14.1075 
 1993 |       63.4000            81.1897          -17.7897 
 1994 |       58.6000            80.7295          -22.1295 
 1995 |       56.4000            78.5023          -22.1023 
 1996 |       54.5000            77.4827          -22.9827 
 1997 |       53.8000            77.7123          -23.9123 
 1998 |       52.3000            74.3976          -22.0976 
 1999 |       47.2000            73.5711          -26.3711 
 2000 |       41.6000            67.3550          -25.7550 
------+----------------------------------------------------
 Mean |       60.3500            79.3518          -19.0018 
-----------------------------------------------------------
```
上表显示，加州控烟法对于人均香烟消费量有很大的负效应，而且此效应随着时间推移而变大，且结果显著，在 1989-2000 年期间，加州的人均年香烟消费逐年减少。

&emsp;
- 输出结果图表如下

![真实加州与合成加州对比](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈少廷_synth2_Figs01.png)
<center>真实加州与合成加州对比</center>
&emsp;

![法案控烟效果](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈少廷_synth2_Figs02.png)
<center>法案控烟效果</center>
&emsp;
由图可得亦可得相同结论。
&emsp;

**4.3安慰剂检验**
```stata
synth2 cigsale lnincome age15to24 retprice beer cigsale(1988) ///
  cigsale(1980) cigsale(1975), trunit(3) trperiod(1989)       ///
  xperiod(1980(1)1988) nested placebo(unit cut(2)) sigf(6)
```
注：1.此处使用 placebo{unit|unit(numlist)} 安慰剂检测方式
2.删除了 allopt 选项，以节省时间。如果时间允许，建议使用 "allopt" 选项以获得最准确的结果
3.为确保收敛，将默认选项 "sigf (7)" (7个有效数字) 改为 "sigf (6)"

**输出结果**
- 输出表格如下
```stata
-------------------------------------------------------------------------------
      Unit     |  Pre MSPE  Post MSPE   Post/Pre MSPE    Pre MSPE of Fake Unit/
               |                                       Pre MSPE of Treated Unit
---------------+---------------------------------------------------------------
    California |    3.1329   390.1416       124.5295                    1.0000 
       Alabama |    5.9124     9.8019         1.6578                    1.8872 
      Arkansas |    4.5588    28.8239         6.3228                    1.4551 
      Colorado |   14.4150    63.8173         4.4271                    4.6011 
   Connecticut |   16.2898   130.8999         8.0357                    5.1996 
      Delaware |   66.0759   693.6159        10.4973                   21.0908 
       Georgia |    1.4815   109.5352        73.9349                    0.4729 
         Idaho |    5.5223    39.6742         7.1844                    1.7627 
      Illinois |    4.5957    21.7398         4.7304                    1.4669 
       Indiana |   14.2862   479.2168        33.5440                    4.5600 
          Iowa |   14.0878    35.0707         2.4894                    4.4967 
        Kansas |   13.8533    16.0592         1.1592                    4.4219 
      Kentucky |  417.0489  1646.9035         3.9489                  133.1180 
     Louisiana |    2.0121   104.8996        52.1337                    0.6423 
         Maine |    8.8795   131.9740        14.8628                    2.8342 
     Minnesota |   15.0262    42.8039         2.8486                    4.7962 
   Mississippi |    4.0275    35.5770         8.8335                    1.2855 
      Missouri |    1.4601    61.8876        42.3848                    0.4661 
       Montana |    5.2861    53.9283        10.2019                    1.6873 
      Nebraska |    4.8856    29.1541         5.9674                    1.5594 
        Nevada |   43.3661    88.1427         2.0325                   13.8420 
  NewHampshire | 3436.5978   134.9018         0.0393                 1096.9292 
     NewMexico |    5.1412    61.9370        12.0471                    1.6410 
 NorthCarolina |   82.5084    58.7052         0.7115                   26.3359 
   NorthDakota |    8.3648    86.2756        10.3141                    2.6700 
          Ohio |    2.3209     5.5309         2.3831                    0.7408 
      Oklahoma |    7.3434   139.7989        19.0374                    2.3439 
  Pennsylvania |    3.2533     4.1222         1.2671                    1.0384 
   RhodeIsland |   75.6025   489.1700         6.4703                   24.1316 
 SouthCarolina |    2.2020    41.1008        18.6655                    0.7028 
   SouthDakota |    6.4177    26.2534         4.0908                    2.0485 
     Tennessee |    5.2043   123.3097        23.6940                    1.6611 
         Texas |    4.6695   239.4218        51.2740                    1.4904 
          Utah |  593.7643   223.2758         0.3760                  189.5239 
       Vermont |   14.4846   135.6750         9.3669                    4.6233 
      Virginia |    2.6367   232.1093        88.0318                    0.8416 
  WestVirginia |    8.0269   245.9066        30.6352                    2.5621 
     Wisconsin |    2.8962    82.1953        28.3802                    0.9244 
       Wyoming |   76.1321    50.1051         0.6581                   24.3006 
-------------------------------------------------------------------------------

----------------------------------------------------------------
 Time |  Treatment Effect      p-value of Treatment Effect      
      |                     Two-sided   Right-sided   Left-sided
------+---------------------------------------------------------
 1989 |          -7.3789       0.0526       1.0000       0.0526 
 1990 |          -9.5041       0.0526       1.0000       0.0526 
 1991 |         -13.2087       0.1579       0.8947       0.1579 
 1992 |         -13.8906       0.1053       0.9474       0.1053 
 1993 |         -17.5725       0.0526       1.0000       0.0526 
 1994 |         -21.9146       0.0526       1.0000       0.0526 
 1995 |         -21.9005       0.0526       1.0000       0.0526 
 1996 |         -22.8023       0.0526       1.0000       0.0526 
 1997 |         -23.7412       0.0526       1.0000       0.0526 
 1998 |         -21.9035       0.1053       0.9474       0.1053 
 1999 |         -26.1703       0.0526       1.0000       0.0526 
 2000 |         -25.5459       0.0526       1.0000       0.0526 
----------------------------------------------------------------
```
- 输出图表如下
![折线图安慰剂检验结果](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈少廷_synth2_Figs03.png)
<center>折线图安慰剂检验结果</center>
&emsp;

在上图中，黑线表示加州的处理效应（即加州与合成加州的人均香烟消费之差），而灰线表示其他39个控制州的安慰剂效应（即这些州与其相应合成州的人均香烟消费之差）。显然，与其他州的安慰剂效应相比，加州的（负）处理效应显得特别大。假如加州的控烟法并无任何效应，则在这39个州中，碰巧看到加州的处理效应最大的概率仅为 1/39 = 0.0256，小于常用的显著性水平 0.05，故初步可知黑线处理效应的确是加州控烟的效果。

![条形图安慰剂检验结果](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈少廷_synth2_Figs04.png)
<center>条形图安慰剂检验结果</center>
&emsp;

![两侧p值](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈少廷_synth2_Figs05.png)
<center>两侧p值</center>
&emsp;

![右侧p值结果](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈少廷_synth2_Figs06.png)
<center>右侧p值结果</center>
&emsp;

![左侧p值结果](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/陈少廷_synth2_Figs07.png)
<center>左侧p值结果</center>
&emsp;

## 5. synth2 命令优点
综上所述， `synth2` 相比于 `synth` ：
- 直接进行安慰剂检验，可选三种检验方式，并且计算出P-value来比较安慰剂检验的效果
- 直接进行稳健性检验和灵敏度检验，并输出结果
- 便捷的可视化呈现估计结果、安慰剂检验和稳健性检验结果

## 6. 参考资料
- Abadie, A. and Gardeazabal, J. 2003. The Economic Costs of Conflict: A Case Study of the Basque Country. American Economic Review. [Link](https://www.aeaweb.org/articles?id=10.1257/000282803321455188)
- Abadie, A., Diamond, A., and J. Hainmueller. 2010. Synthetic Control Methods for Comparative Case Studies: Estimating the Effect of California's Tobacco Control Program. Journal of the American Statistical Association. [Link](https://xueshu.baidu.com/usercenter/paper/show?paperid=5b788ba1d4c288a4afea8e62b21c9154&site=xueshu_se&sc_from=pku)
- Abadie, A., Diamond, A., and J. Hainmueller. 2014. Comparative Politics and the Synthetic Control Method. American Journal of Political Science. [Link](https://xueshu.baidu.com/usercenter/paper/show?paperid=1s3s0p50mm780cn0km0n0gb08w261726&site=xueshu_se&sc_from=pku)
- Robert McClelland., and Sarah Gault. 2017. The Synthetic Control Method as a Tool to Understand State Policy. [Link](https://www.taxpolicycenter.org/publications/synthetic-control-method-tool-understand-state-policy/full)
- xuyiqing，GitHub项目，Generalized Synthetic Control Method / gsynth [Link](https://github.com/xuyiqing/gsynth)

## 7. 相关推文
- 专题：[连享会合成控制法](https://www.lianxh.cn/blogs/42.html)
    - 何庆红，连享会推文，[合成控制法 (Synthetic Control Method) 及 Stata实现](https://www.lianxh.cn/news/9e1bb97a57041.html)
    - 陈勇吏，连享会推文，[Stata：合成控制法-synth-命令无法加载-plugin-的解决办法](https://www.lianxh.cn/news/d7c94c7777226.html)
    - 专题整理，[Stata：合成控制法程序分享](https://www.lianxh.cn/news/bf1839debd082.html)
    - 何庆红，连享会推文， [Synth_Runner命令：合成控制法高效实现](https://www.lianxh.cn/news/db3d2785cd5c0.html)

- 文献整理，[Stata-合成控制法：一组文献](https://zhuanlan.zhihu.com/p/36709180)
